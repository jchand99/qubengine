# QubEngine

2D-3D graphics/game engine written in C# using Magma library.

## Directory Structure:
- Qube: Sandbox application using the engine.
- QubEngine: Engine itself.
