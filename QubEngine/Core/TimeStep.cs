namespace QubEngine.Core
{
    public struct TimeStep
    {
        public float Seconds { get; private set; }
        public float Milliseconds
        {
            get
            {
                return Seconds * 1000.0f;
            }
        }

        public TimeStep(float time = 0.0f) { Seconds = time; }

        public static implicit operator TimeStep(float time) => new TimeStep(time);
        public static implicit operator float(TimeStep timeStep) => timeStep.Seconds;
    }
}
