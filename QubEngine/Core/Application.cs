using System;

using Magma.Logger;
using Magma.GLFW;

using QubEngine.Platform.OS;
using QubEngine.Core.Events;
using QubEngine.Core.Layers;
using QubEngine.Core.Rendering;

namespace QubEngine.Core
{
    public class Application
    {
        private bool _Running;
        private bool _Minimized;
        private float _LastFrame;
        private float _FrameTimer;
        private uint _FrameCount;
        private LayerStack _LayerStack;
        public Window Window { get; private set; }
        public static Application Instance { get; private set; }
        public static int Width { get { return Instance.Window.Width; } }
        public static int Height { get { return Instance.Window.Height; } }

        public Application(string title)
        {
            Init(title);
            Instance = this;
            Log.AssertNotNull<Application>(Instance, "Application already exists!");
        }

        private void Init(string title)
        {
            Log.AddLogger(new ConsoleLogger());

            Window = Window.Create(new WindowProps(title, 1920, 1080));
            if (Window == null || !Window.IsValid)
            {
                Log.Fatal<Application>("Null or invalid window!");
                Glfw.Terminate();
                Environment.Exit(-1);
            }
            Window.SetEventCallback(OnEvent);
            Window.SetVSync(false);
            Window.HideCursor();

            _Running = true;
            _Minimized = false;
            _LastFrame = 0.0f;

            _LayerStack = new LayerStack();

            Renderer.Init();
        }

        public void Run()
        {
            Loop();
            CleanUp();
        }

        private void Loop()
        {
            while (_Running)
            {
                _FrameCount++;
                float time = (float)Glfw.GetTime();
                TimeStep ts = time - _LastFrame;
                _LastFrame = time;
                _FrameTimer += ts;

                if (_FrameTimer >= 1.0f)
                {
                    Log.Info<Application>($"[FPS: {_FrameCount}, TPS: {1000.0f / _FrameCount}]");
                    _FrameTimer = 0.0f;
                    _FrameCount = 0;
                }

                Window.OnUpdate();

                for (int i = _LayerStack.Count - 1; i >= 0; i--)
                {
                    _LayerStack[i].OnUpdate(ts);
                }
            }
        }

        public void PushLayer(Layer layer)
        {
            _LayerStack.PushLayer(layer);
            layer.OnAttach();
        }

        public void PushOverlya(Layer layer)
        {
            _LayerStack.PushOverlay(layer);
            layer.OnAttach();
        }

        private void OnEvent(Event @event)
        {
            EventDispatcher dispatcher = new EventDispatcher(@event);
            dispatcher.Dispatch<WindowCloseEvent>(OnClose);
            dispatcher.Dispatch<KeyEvent>(OnKeyPressed);
            dispatcher.Dispatch<WindowResizeEvent>(OnWindowResize);

            if (!_Minimized)
            {
                for (int i = _LayerStack.Count - 1; i >= 0; i--)
                {
                    if (@event.Handled)
                        break;
                    _LayerStack[i].OnEvent(@event);
                }
            }
        }

        private void CleanUp()
        {
            Renderer.Shutdown();
            _LayerStack.CleanUp();
            Window.Close();
            Log.Debug<Application>("Cleaned up Application!");
        }

        private bool OnKeyPressed(KeyEvent e)
        {
            if (e.Key == Key.Escape) _Running = false;
            return false;
        }

        private bool OnClose(WindowCloseEvent e)
        {
            _Running = false;
            Log.Debug<Application>(e.ToString());
            return true;
        }

        private bool OnWindowResize(WindowResizeEvent arg)
        {
            if (arg.Width == 0 || arg.Height == 0)
            {
                _Minimized = true;
                return false;
            }

            _Minimized = false;
            Renderer.OnWindowResize(0, 0, arg.Width, arg.Height);
            return false;
        }

    }
}
