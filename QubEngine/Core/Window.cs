using System;
using Magma.Logger;
using QubEngine.Core.Events;
using QubEngine.Platform.OS.Windows;
using QubEngine.Platform.OS.Linux;
using QubEngine.Platform.OS.Mac;
using static System.Runtime.InteropServices.OSPlatform;
using static System.Runtime.InteropServices.RuntimeInformation;

namespace QubEngine.Core
{
    public abstract class Window
    {
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        public bool IsVsync { get; protected set; }
        public bool IsValid { get; protected set; }
        public string Title { get; protected set; }
        public IntPtr Handle { get; protected set; }

        protected int WindowCount;

        public abstract void OnUpdate();
        public abstract void SetEventCallback(Action<Event> callback);
        public abstract void SetVSync(bool enabled);
        public abstract void Close();

        public virtual void EnableCursor() { }
        public virtual void HideCursor() { }
        public virtual void DisableCursor() { }

        public static Window Create(WindowProps props)
        {
            if (IsOSPlatform(Windows)) return new WindowsWindow(props);
            else if (IsOSPlatform(Linux)) return new LinuxWindow(props);
            else if (IsOSPlatform(OSX)) return new MacWindow(props);
            else throw new QubEngineCoreException("Platform not supported! Cannot create a window for your platform!");
        }
    }

    public struct WindowProps
    {
        public string Title;
        public int Width;
        public int Height;

        public WindowProps(string title, int width = 1280, int height = 720)
        {
            Title = title;
            Width = width;
            Height = height;
        }
    }
}
