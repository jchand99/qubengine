using Magma.Maths;

namespace QubEngine.Core
{
    public abstract class Input
    {
        private static Input _Instance { get; set; }

        internal static void Init(Input input) => _Instance = input;

        public static bool IsKeyPressed(Key key) => _Instance.IsKeyPressedImpl(key);
        public static bool IsMouseButtonPressed(MButton button) => _Instance.IsMouseButtonPressedImpl(button);
        public static Vector2f GetMousePositioin() => _Instance.GetMousePositionImpl();
        public static float GetMouseX() => _Instance.GetMouseXImpl();
        public static float GetMouseY() => _Instance.GetMouseYImpl();

        public abstract bool IsKeyPressedImpl(Key key);
        public abstract bool IsMouseButtonPressedImpl(MButton button);
        public abstract Vector2f GetMousePositionImpl();
        public abstract float GetMouseXImpl();
        public abstract float GetMouseYImpl();
    }
}
