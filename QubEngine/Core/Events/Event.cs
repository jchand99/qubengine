using System;

namespace QubEngine.Core.Events
{
    public class Event
    {
        public bool Handled { get; set; }
        public string Name { get; protected set; }
        public EventType Type { get; protected set; }
        public EventCategory CategoryFlags { get; protected set; }

        public bool IsInCategory(EventCategory category) => CategoryFlags.HasFlag(category);
    }

    public struct EventDispatcher
    {
        private Event _Event;

        public EventDispatcher(Event @event) { _Event = @event; }

        public bool Dispatch<TEvent>(Func<TEvent, bool> func) where TEvent : Event
        {
            if (_Event is TEvent)
            {
                _Event.Handled |= func((TEvent)_Event);
                return true;
            }
            return false;
        }
    }

    public enum EventType
    {
        None = 0,
        WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
        KeyPressed, KeyReleased, KeyTyped,
        MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
    }

    [Flags]
    public enum EventCategory
    {
        None = 0,
        Application,
        Input,
        Keyboard,
        Mouse,
        MouseButton
    }
}
