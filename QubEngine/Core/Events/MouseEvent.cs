using System;
using Magma.GLFW;

namespace QubEngine.Core.Events
{
    public class MouseMovedEvent : Event
    {
        public float X { get; private set; }
        public float Y { get; private set; }

        public MouseMovedEvent(float x, float y) : base()
        {
            X = x;
            Y = y;
            Type = EventType.MouseMoved;
            CategoryFlags = EventCategory.Mouse | EventCategory.Input;
        }

        public override string ToString()
        {
            return $"MouseMovedEvent: {X}, {Y}";
        }
    }

    public class MouseScrolledEvent : Event
    {
        public float XOffset { get; protected set; }
        public float YOffset { get; protected set; }

        public MouseScrolledEvent(float xOffset, float yOffset) : base()
        {
            XOffset = xOffset;
            YOffset = yOffset;
            Type = EventType.MouseScrolled;
            CategoryFlags = EventCategory.Mouse | EventCategory.Input;
        }

        public override string ToString()
        {
            return $"MouseScrolledEvent: {XOffset}, {YOffset}";
        }
    }

    public class MouseButtonEvent : Event
    {
        public MouseButton Button { get; protected set; }

        public MouseButtonEvent(MouseButton mouseButton)
        {
            Button = mouseButton;
            CategoryFlags = EventCategory.Mouse | EventCategory.Input | EventCategory.MouseButton;
        }
    }

    public class MouseButtonPressedEvent : MouseButtonEvent
    {
        public MouseButtonPressedEvent(MouseButton mouseButton) : base(mouseButton)
        {
            Button = mouseButton;
            Type = EventType.MouseButtonPressed;
        }

        public override string ToString()
        {
            return $"MouseButtonPressedEvent: {Button}";
        }
    }

    public class MouseButtonReleasedEvent : MouseButtonEvent
    {
        public MouseButtonReleasedEvent(MouseButton mouseButton) : base(mouseButton)
        {
            Button = mouseButton;
            Type = EventType.MouseButtonReleased;
        }

        public override string ToString()
        {
            return $"MouseButtonReleasedEvent: {Button}";
        }
    }
}
