namespace QubEngine.Core.Events
{
    public class WindowResizeEvent : Event
    {
        public int Width { get; private set; }
        public int Height { get; private set; }

        public WindowResizeEvent(int width, int height) : base()
        {
            Width = width;
            Height = height;
            Type = EventType.WindowResize;
            CategoryFlags = EventCategory.Application;
        }

        public override string ToString()
        {
            return $"WindowResizeEvent: {Width}, {Height}";
        }
    }

    public class WindowCloseEvent : Event
    {
        public WindowCloseEvent() : base()
        {
            Type = EventType.WindowClose;
            CategoryFlags = EventCategory.Application;
        }

        public override string ToString()
        {
            return "WindowClosedEvent";
        }
    }
}
