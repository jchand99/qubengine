using System;
using Magma.GLFW;

namespace QubEngine.Core.Events
{
    public class KeyEvent : Event
    {
        public Key Key { get; protected set; }

        public KeyEvent(Key key)
        {
            CategoryFlags = EventCategory.Keyboard | EventCategory.Input;
            Key = key;
        }
    }

    public class KeyPressedEvent : KeyEvent
    {
        public int RepeatCount { get; private set; }

        public KeyPressedEvent(Key key, int repeatCount) : base(key)
        {
            Key = key;
            Type = EventType.KeyPressed;
            RepeatCount = repeatCount;
        }

        public override string ToString()
        {
            return $"KeyPressedEvent: {Key} ({RepeatCount} repeats)";
        }
    }

    public class KeyReleasedEvent : KeyEvent
    {
        public KeyReleasedEvent(Key key) : base(key)
        {
            Type = EventType.KeyReleased;
        }

        public override string ToString()
        {
            return $"KeyReleasedEvent: {Key}";
        }
    }

    public class KeyTypedEvent : KeyEvent
    {
        public KeyTypedEvent(Key key) : base(key)
        {
            Type = EventType.KeyTyped;
        }

        public override string ToString()
        {
            return $"KeyTypedEvent: {Key}";
        }
    }
}
