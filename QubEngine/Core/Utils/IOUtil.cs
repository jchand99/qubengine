using System;
using System.IO;

namespace QubEngine.Core.Utils
{
    public static class IOUtil
    {
        public static ReadOnlySpan<char> ReadAllText(string directory, string fileName)
        {
            if (!Directory.Exists(directory)) throw new IOException($"Directory '{directory}' does not exist!");
            string path = Path.Combine(directory, fileName);
            if (!File.Exists(path)) throw new IOException($"File '{fileName}' does not exist!");
            return File.ReadAllText(path).AsSpan();
        }

        public static ReadOnlySpan<char> ReadAllText(string path)
        {
            if (!File.Exists(path)) throw new IOException($"File '{path}' does not exist!");
            return File.ReadAllText(path).AsSpan();
        }

        public static byte[] ReadAllBytes(string path)
        {
            if (!File.Exists(path)) throw new IOException($"File '{path}' does not exist!");
            return File.ReadAllBytes(path);
        }

        public static string[] ReadAllLines(string directory, string fileName)
        {
            if (!Directory.Exists(directory)) throw new IOException("Directory does not exist!");
            string path = Path.Combine(directory, fileName);
            if (!File.Exists(path)) throw new IOException($"File '{fileName}' in directory '{directory}' does not exist!");
            return File.ReadAllLines(path);
        }

        public static string[] ReadAllLines(string path)
        {
            if (!File.Exists(path)) throw new IOException($"File '{path}' does not exist!");
            return File.ReadAllLines(path);
        }

        public static string Combine(string directory, string fileName) => Path.Combine(directory, fileName);
    }
}
