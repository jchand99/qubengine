using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public static class PlatformCommand
    {
        private static RendererAPI _SelectedAPI = RendererAPI.Create();

        public static void Clear() => _SelectedAPI.Clear();
        public static void ClearColor(Vector4f color)
        {
            _SelectedAPI.SetClearColor(color);
        }

        public static void SetViewport(int x, int y, int width, int height)
        {
            _SelectedAPI.SetViewport(x, y, width, height);
        }

        public static void DrawIndexed(VertexArray vertexArray, uint count = 0)
        {
            _SelectedAPI.DrawIndexed(vertexArray, count);
        }
    }
}
