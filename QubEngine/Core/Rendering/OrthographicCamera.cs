using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public class OrthographicCamera : Camera
    {
        public OrthographicCamera(float left, float right, float bottom, float top, float near, float far)
        {
            Projection = Matrix4f.Orthographic(left, right, bottom, top, near, far);
            ProjectionView = Projection * View;
        }

        public override void SetProjection(float left, float right, float bottom, float top)
        {
            base.SetProjection(left, right, bottom, top);
            RecalculateOrthographicViewMatrix();
        }

        public override void SetPosition(Vector3f position)
        {
            base.SetPosition(position);
            RecalculateOrthographicViewMatrix();
        }

        public override void SetRotation(float radians)
        {
            base.SetRotation(radians);
            RecalculateOrthographicViewMatrix();
        }

        private void RecalculateOrthographicViewMatrix()
        {
            View = Matrix4f.Translation(Position).Rotate(Rotation, new Vector3f(0.0f, 0.0f, 1.0f)).Inverse();
            ProjectionView = Projection * View;
        }
    }
}
