using System;

namespace QubEngine.Core.Rendering
{
    public class Mesh : IDisposable
    {
        private bool disposedValue;

        public VertexArray VertexArray { get; protected set; }
        public VertexBuffer VertexBuffer { get; protected set; }
        public IndexBuffer IndexBuffer { get; protected set; }
        public Material Material { get; protected set; }
        public uint IndicesCount { get; protected set; }

        public Mesh(Material material)
        {
            Material = material;
            VertexArray = VertexArray.Create();
            VertexArray.Unbind();
        }

        public virtual void AddData(Span<float> vertices, Span<uint> indices, BufferLayout layout)
        {
            VertexArray.Bind();
            uint sizeInBytes = (uint)(vertices.Length * sizeof(float));
            VertexBuffer = VertexBuffer.Create(sizeInBytes);
            VertexBuffer.Layout = layout;
            VertexBuffer.SetData<float>(vertices, sizeInBytes);
            VertexArray.AddVertexBuffer(VertexBuffer);
            VertexBuffer.Unbind();

            IndicesCount = (uint)indices.Length;
            IndexBuffer = IndexBuffer.Create(indices);
            VertexArray.SetIndexBuffer(IndexBuffer);
            IndexBuffer.Unbind();
            VertexArray.Unbind();
        }

        public virtual void Destroy()
        {
            VertexBuffer.Dispose();
            IndexBuffer.Dispose();
            VertexArray.Dispose();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing) { }
                Destroy();
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~Mesh()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
