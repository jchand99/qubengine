using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public class Renderer3D
    {
        internal class Data
        {
            internal const uint MaxCubes = 100000;
            internal const uint MaxVertices = MaxCubes * 24;
            internal const uint MaxIndices = MaxCubes * 36;
            internal const uint MaxTextureSlots = 32; // TODO: RendererCaps

            internal VertexArray QuadVertexArray;
            internal VertexBuffer QuadVertexBuffer;
            internal IndexBuffer QuadIndexBuffer;
            internal Shader TextureShader;
            internal Texture2D WhiteTexture;
            internal static readonly Vector4f WhiteColor = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

            internal int VerticesCount { get; set; }
            internal uint IndexCount { get; set; } = 0;
            internal Vertex[] Vertices = new Vertex[MaxVertices];

            internal Texture2D[] TextureSlots = new Texture2D[MaxTextureSlots];
            internal uint TextureSlotIndex = 1; // 0 = WhiteTexture;

            internal Vector4f[] QuadVertexPositions = new Vector4f[24];
            internal Vector2f[] TexCoords = new Vector2f[24];
        }

        private static Data _Data;
        public static int DrawCalls { get; set; }

        public static void Init()
        {
            _Data = new Data();
            _Data.QuadVertexArray = VertexArray.Create();
            _Data.QuadVertexArray.Bind();

            _Data.QuadVertexBuffer = VertexBuffer.Create(Data.MaxVertices * Vertex.SizeInBytes);
            _Data.QuadVertexBuffer.Layout = new BufferLayout()
            {
                { ShaderDataType.Float3, "a_Position" },
                { ShaderDataType.Float4, "a_Color" },
                { ShaderDataType.Float2, "a_TexCoords" },
                { ShaderDataType.Float, "a_TexIndex" },
                { ShaderDataType.Float, "a_TilingFactor" },
            };

            uint[] indices = new uint[Data.MaxIndices];

            uint offset = 0;
            for (int i = 0; i < Data.MaxIndices; i += 6)
            {
                indices[i + 0] = offset + 0;
                indices[i + 1] = offset + 1;
                indices[i + 2] = offset + 2;

                indices[i + 3] = offset + 2;
                indices[i + 4] = offset + 3;
                indices[i + 5] = offset + 0;
                offset += 4;
            }

            _Data.QuadIndexBuffer = IndexBuffer.Create(indices);

            _Data.QuadVertexArray.AddVertexBuffer(_Data.QuadVertexBuffer);
            _Data.QuadVertexArray.SetIndexBuffer(_Data.QuadIndexBuffer);
            _Data.QuadVertexBuffer.Unbind();
            _Data.QuadIndexBuffer.Unbind();
            _Data.QuadVertexArray.Unbind();

            int[] samplers = new int[Data.MaxTextureSlots];
            for (int i = 0; i < Data.MaxTextureSlots; i++) samplers[i] = i;

            _Data.TextureShader = Shader.Create("assets/shaders/3DTextureShader.glsl");
            _Data.TextureShader.Bind();
            _Data.TextureShader.Upload("u_Textures", samplers);
            _Data.TextureShader.Unbind();

            _Data.WhiteTexture = Texture2D.Create(1, 1);
            _Data.WhiteTexture.SetData<uint>(0xffffffff, sizeof(uint));

            _Data.TextureSlots[0] = _Data.WhiteTexture;

            // Front
            _Data.QuadVertexPositions[0] = new Vector4f(-0.5f, -0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[1] = new Vector4f(0.5f, -0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[2] = new Vector4f(0.5f, 0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[3] = new Vector4f(-0.5f, 0.5f, 0.5f, 1.0f);

            // Right
            _Data.QuadVertexPositions[4] = new Vector4f(0.5f, -0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[5] = new Vector4f(0.5f, -0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[6] = new Vector4f(0.5f, 0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[7] = new Vector4f(0.5f, 0.5f, 0.5f, 1.0f);

            // Back
            _Data.QuadVertexPositions[8] = new Vector4f(0.5f, -0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[9] = new Vector4f(-0.5f, -0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[10] = new Vector4f(-0.5f, 0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[11] = new Vector4f(0.5f, 0.5f, -0.5f, 1.0f);

            // Left
            _Data.QuadVertexPositions[12] = new Vector4f(-0.5f, -0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[13] = new Vector4f(-0.5f, -0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[14] = new Vector4f(-0.5f, 0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[15] = new Vector4f(-0.5f, 0.5f, -0.5f, 1.0f);

            // Top
            _Data.QuadVertexPositions[16] = new Vector4f(-0.5f, 0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[17] = new Vector4f(0.5f, 0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[18] = new Vector4f(0.5f, 0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[19] = new Vector4f(-0.5f, 0.5f, -0.5f, 1.0f);

            // Bottom
            _Data.QuadVertexPositions[20] = new Vector4f(-0.5f, -0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[21] = new Vector4f(0.5f, -0.5f, -0.5f, 1.0f);
            _Data.QuadVertexPositions[22] = new Vector4f(0.5f, -0.5f, 0.5f, 1.0f);
            _Data.QuadVertexPositions[23] = new Vector4f(-0.5f, -0.5f, 0.5f, 1.0f);

            for (int i = 0; i < _Data.TexCoords.Length; i += 4)
            {
                _Data.TexCoords[i] = new Vector2f(0.0f, 0.0f);
                _Data.TexCoords[i + 1] = new Vector2f(1.0f, 0.0f);
                _Data.TexCoords[i + 2] = new Vector2f(1.0f, 1.0f);
                _Data.TexCoords[i + 3] = new Vector2f(0.0f, 1.0f);
            }
        }

        public static void Shutdown()
        {
            _Data.TextureShader.Dispose();
            _Data.WhiteTexture.Dispose();
            _Data.QuadVertexArray.Dispose();
        }

        public static void BeginScene(Camera camera)
        {
            var projectionView = camera.ProjectionView;

            _Data.TextureShader.Bind();
            _Data.QuadVertexArray.Bind();
            _Data.QuadIndexBuffer.Bind();
            _Data.QuadVertexBuffer.Bind();
            for (int i = 0; i < _Data.TextureSlotIndex; i++) _Data.TextureSlots[i].Bind((uint)i);

            _Data.TextureShader.Upload("u_ProjectionView", ref projectionView);
            StartBatch();
        }
        public static void EndScene()
        {
            Flush();

            for (int i = 0; i < _Data.TextureSlotIndex; i++) _Data.TextureSlots[i].Unbind((uint)i);
            _Data.QuadVertexBuffer.Unbind();
            _Data.QuadIndexBuffer.Unbind();
            _Data.QuadVertexArray.Unbind();
            _Data.TextureShader.Unbind();
        }

        private static void Flush()
        {
            DrawCalls++;
            _Data.QuadVertexBuffer.SetData<float>(ref _Data.Vertices[0].Position.X, (uint)(_Data.Vertices.Length * Vertex.SizeInBytes));
            PlatformCommand.DrawIndexed(_Data.QuadVertexArray, _Data.IndexCount);
        }

        private static void StartBatch()
        {
            _Data.TextureSlotIndex = 1;
            _Data.VerticesCount = -1;
            _Data.IndexCount = 0;
        }

        private static void NextBatch()
        {
            Flush();
            StartBatch();
        }

        public static void DrawCubeFace(Vector3f position, Vector3f size, Vector4f color, CubeFace face)
        {
            if (_Data.IndexCount >= Data.MaxIndices)
                NextBatch();

            const int quadVertexCount = 4;
            const float textureIndex = 0.0f;
            const float tilingFactor = 1.0f;

            int offset = (int)face;

            Matrix4f transform = Matrix4f.Translation(position).Scale(size);
            for (int i = offset; i < offset + quadVertexCount; i++)
            {
                Vertex vertex;
                vertex.Color = color;
                vertex.Position = transform * _Data.QuadVertexPositions[i];
                vertex.TexCoords = _Data.TexCoords[i];
                vertex.TexIndex = textureIndex;
                vertex.TilingFactor = tilingFactor;

                _Data.Vertices[++_Data.VerticesCount] = vertex;
            }
            _Data.IndexCount += 6;
        }

        public static void DrawCube(Vector3f position, Vector3f size, Vector4f color)
        {
            const int cubeVertexCount = 24;
            const float textureIndex = 0.0f;
            const float tilingFactor = 1.0f;

            if (_Data.IndexCount >= Data.MaxIndices)
                NextBatch();

            Matrix4f transform = Matrix4f.Translation(position).Scale(size);

            for (int i = 0; i < cubeVertexCount; i++)
            {
                Vertex vertex;
                vertex.Color = color;
                vertex.Position = transform * _Data.QuadVertexPositions[i];
                vertex.TexCoords = _Data.TexCoords[i];
                vertex.TexIndex = textureIndex;
                vertex.TilingFactor = tilingFactor;

                _Data.Vertices[++_Data.VerticesCount] = vertex;
            }
            _Data.IndexCount += 36;
        }

        public static void DrawRotatedCube(Vector3f position, Vector3f size, Vector4f color, float radians, Vector3f rotation)
        {
            const int cubeVertexCount = 24;
            const float textureIndex = 0.0f;
            const float tilingFactor = 1.0f;

            if (_Data.IndexCount >= Data.MaxIndices)
                NextBatch();

            Matrix4f transform = Matrix4f.Translation(position).Rotate(radians, rotation).Scale(size);

            for (int i = 0; i < cubeVertexCount; i++)
            {
                Vertex vertex;
                vertex.Color = color;
                vertex.Position = transform * _Data.QuadVertexPositions[i];
                vertex.TexCoords = _Data.TexCoords[i];
                vertex.TexIndex = textureIndex;
                vertex.TilingFactor = tilingFactor;

                _Data.Vertices[++_Data.VerticesCount] = vertex;
            }
            _Data.IndexCount += 36;
        }
    }

    public enum CubeFace : uint
    {
        Front = 0,
        Right = 4,
        Back = 8,
        Left = 12,
        Top = 16,
        Bottom = 20,
    }
}
