using System;
using QubEngine.Platform.Graphics.OpenGL;

namespace QubEngine.Core.Rendering
{

    public abstract class Texture
    {
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        protected uint _TextureID;
        public uint TextureID { get { return _TextureID; } protected set { _TextureID = value; } }

        public abstract void SetData<T>(Span<T> data) where T : unmanaged;
        public abstract void SetData<T>(ReadOnlySpan<T> data) where T : unmanaged;
        public abstract void SetData<T>(T data, int size) where T : unmanaged;

        public abstract void Bind(uint slot = 0);
        public abstract void Unbind(uint slot = 0);

    }

    public abstract class Texture2D : Texture, IDisposable
    {
        private bool disposedValue;

        public static Texture2D Create(int width, int height)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLTexture2D(width, height),
                _ => throw new QubEngineException("Selected rendering API does not exist!")
            };
        }
        public static Texture2D Create(string path)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLTexture2D(path),
                _ => throw new QubEngineException("Selected rendering API does not exist!")
            };
        }

        public abstract override void SetData<T>(Span<T> data);
        public abstract override void SetData<T>(ReadOnlySpan<T> data);
        public abstract override void SetData<T>(T data, int size);

        public abstract override void Bind(uint slot = 0);
        public abstract override void Unbind(uint slot = 0);
        protected abstract void Destroy();

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing) { }

                Destroy();
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~Texture2D()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
