using System;
using QubEngine.Platform.Graphics.OpenGL;

namespace QubEngine.Core.Rendering
{
    public struct FramebufferSpecification
    {
        public uint Width;
        public uint Height;
        public uint Samples;
        public bool SwapchainTarget;

        public FramebufferSpecification(uint width = 0, uint height = 0, uint samples = 1, bool swapchainTarget = false)
        {
            Width = width;
            Height = height;
            Samples = samples;
            SwapchainTarget = swapchainTarget;
        }
    }

    public abstract class Framebuffer : IDisposable
    {
        protected FramebufferSpecification _Specification;
        private bool disposedValue;

        public FramebufferSpecification Specification
        {
            get { return _Specification; }
        }
        public static Framebuffer Create(FramebufferSpecification specification)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLFramebuffer(specification),
                _ => throw new QubEngineException(),
            };
        }

        public abstract void Bind();
        public abstract void Unbind();

        public abstract void Resize(uint width, uint height);
        public abstract uint GetColorAttachmentRendererID();

        protected abstract void Destroy();
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing) { }

                Destroy();
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~Framebuffer()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
