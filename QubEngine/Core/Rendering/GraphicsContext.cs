using System;
using QubEngine.Platform.Graphics.OpenGL;
using QubEngine.Platform.Graphics.Vulkan;

namespace QubEngine.Core.Rendering
{
    public abstract class GraphicsContext
    {
        public static GraphicsContext Create(IntPtr windowHandle)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLContext(windowHandle),
                API.Vulkan => new VulkanContext(windowHandle),
                _ => throw new QubEngineException("Selected rendering API does not exist!"),
            };
        }

        public abstract void SwapBuffers();

    }
}
