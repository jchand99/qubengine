using System;
using Magma.Maths;
using System.Threading;
using System.Collections;
using Magma.Logger;


namespace QubEngine.Core.Rendering
{
    public class Renderer
    {
        private static SceneData _SceneData;
        // static Queue Queue;
        // static bool IsRunning { get; set; }
        // static Thread Thread;

        public static void Init()
        {
            // Queue = Queue.Synchronized(new Queue());
            // Thread = new Thread(RunQueue);

            // IsRunning = true;
            // Thread.Start();
            Renderer2D.Init();
        }
        public static void Shutdown()
        {
            // IsRunning = false;
            // Thread.Join();
            Renderer2D.Shutdown();
        }

        public static void OnWindowResize(int x, int y, int width, int height)
        {
            PlatformCommand.SetViewport(x, y, width, height);
        }

        public static void BeginScene(CameraController controller) => BeginScene(controller.Camera);
        public static void BeginScene(Camera camera) { _SceneData.ProjectionView = camera.ProjectionView; }
        public static void EndScene() { }

        public static void Submit(Mesh mesh, ref Matrix4f transform)
        {
            var t = transform;
            // Action f = () =>
            // {
            mesh.Material.BindShader();
            mesh.Material.Set("u_ProjectionView", ref _SceneData.ProjectionView);
            mesh.Material.Set("u_Transform", ref t);
            mesh.VertexArray.Bind();
            mesh.IndexBuffer.Bind();
            PlatformCommand.DrawIndexed(mesh.VertexArray, mesh.IndicesCount);
            mesh.IndexBuffer.Unbind();
            mesh.VertexArray.Unbind();
            mesh.Material.UnbindShader();
            // };
            // Queue.Enqueue(f);
            // Render();
        }

        // private static void Render(Action action)
        // {
        //     action?.Invoke();
        // }

        // static void RunQueue()
        // {
        //     while (IsRunning)
        //     {
        //         Action func = null;
        //         lock (Queue.SyncRoot)
        //         {
        //             if (Queue.Count > 0)
        //             {
        //                 // Log.Debug<RenderQueue>($"{Queue.Count}");
        //                 func = Queue.Dequeue() as Action;
        //             }
        //         }
        //         // Log.Info<RenderQueue>(func?.ToString());
        //         func?.Invoke();
        //     }
        // }

        // public static void Submit(Shader shader, VertexArray vertexArray, Texture2D texture, ref Matrix4f transform)
        // {
        //     if (shader == null || texture == null) return;

        //     shader.Bind();
        //     texture.Bind();
        //     shader.Upload("u_ProjectionView", ref _SceneData.ProjectionView);
        //     shader.Upload("u_Transform", ref transform);
        //     shader.Upload("u_Texture", 0);
        //     vertexArray.Bind();

        //     ApiCommand.DrawIndexed(vertexArray, vertexArray.GetIndexBufferCount());
        // }
    }

    internal struct SceneData
    {
        internal Matrix4f ProjectionView;
    }
}
