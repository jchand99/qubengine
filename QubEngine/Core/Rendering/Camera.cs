using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public abstract class Camera
    {
        public virtual Matrix4f Projection { get; protected set; }
        public virtual Matrix4f View { get; protected set; } = Matrix4f.Identity;
        public virtual Matrix4f ProjectionView { get; protected set; }

        public virtual Vector3f Position { get; protected set; }
        public virtual float Rotation { get; protected set; }

        public virtual void SetProjection(float fov, float aspectRatio)
        {
            Projection = Matrix4f.Perspective(fov, aspectRatio, 0.1f, 100.0f);
        }

        public virtual void SetProjection(float left, float right, float bottom, float top)
        {
            Projection = Matrix4f.Orthographic(left, right, bottom, top, -1.0f, 1.0f);
        }

        public virtual void SetPosition(Vector3f position)
        {
            if (Position == position) return;
            Position = position;
        }

        public virtual void SetRotation(float radians)
        {
            if (Rotation == radians) return;
            Rotation = radians;
        }

        public virtual void SetPerspective(Vector3f front, Vector3f up) { }

        public virtual void SetZoom(float zoom) { }
    }
}
