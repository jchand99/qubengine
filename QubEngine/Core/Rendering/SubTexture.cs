using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public class SubTexture2D
    {
        public Texture2D Texture { get; private set; }
        private Vector2f[] _TextureCoordinates = new Vector2f[4];

        public Vector2f this[int index]
        {
            get { return _TextureCoordinates[index]; }
        }

        private SubTexture2D(Texture2D texture, Vector2f min, Vector2f max)
        {
            Texture = texture;
            _TextureCoordinates = new Vector2f[]
            {
                new(min.X, min.Y),
                new(max.X, min.Y),
                new(max.X, max.Y),
                new(min.X, max.Y)
            };
        }

        public static SubTexture2D CreateFromCoordinates(Texture2D texture, Vector2f coordinates, Vector2f cellSize, Vector2f spriteSize)
        {
            Vector2f min = new((coordinates.X * cellSize.X) / texture.Width, (coordinates.Y * cellSize.Y) / texture.Height);
            Vector2f max = new(((coordinates.X + spriteSize.X) * cellSize.X) / texture.Width, ((coordinates.Y + spriteSize.Y) * cellSize.Y) / texture.Height);
            return new SubTexture2D(texture, min, max);
        }
    }
}
