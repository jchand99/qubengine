using System;
using QubEngine.Platform.Graphics.OpenGL;

namespace QubEngine.Core.Rendering
{
    public abstract class VertexArray : IDisposable
    {
        private bool disposedValue;

        public static VertexArray Create()
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLVertexArray(),
                _ => throw new QubEngineException("Selected rendering API does not exist!"),
            };
        }

        public abstract void Bind();
        public abstract void Unbind();
        public abstract void AddVertexBuffer(VertexBuffer buffer);
        public abstract void SetIndexBuffer(IndexBuffer buffer);
        public abstract uint GetIndexBufferCount();

        public abstract void Destroy();

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                Destroy();
                disposedValue = true;
            }
        }

        ~VertexArray()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
