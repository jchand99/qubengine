using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public class Material
    {
        private Shader _Shader;

        public Vector4f Color { get; set; }

        public Material(Shader shader)
        {
            _Shader = shader;
        }

        public void Set(string name, float value) => _Shader.Upload(name, value);
        public void Set(string name, ref Vector3f values) => _Shader.Upload(name, ref values);
        public void Set(string name, ref Vector4f values) => _Shader.Upload(name, ref values);
        public void Set(string name, int value) => _Shader.Upload(name, value);
        public void Set(string name, int[] values) => _Shader.Upload(name, values);
        public void Set(string name, ref Matrix3f matrix) => _Shader.Upload(name, ref matrix);
        public void Set(string name, ref Matrix4f matrix) => _Shader.Upload(name, ref matrix);

        public void BindShader() => _Shader.Bind();
        public void UnbindShader() => _Shader.Unbind();
    }
}
