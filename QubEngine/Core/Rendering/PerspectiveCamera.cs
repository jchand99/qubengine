using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public class PerspectiveCamera : Camera
    {
        private Vector3f _Front;
        private Vector3f _Up;

        public PerspectiveCamera(float fov, float aspectRatio, float near, float far)
        {
            _Front = new Vector3f(0.0f, 0.0f, -1.0f);
            Projection = Matrix4f.Perspective(fov, aspectRatio, near, far);

            View = Matrix4f.LookAt(Position, Position + _Front, Vector3f.Up);
            ProjectionView = Projection * View;
        }

        public override void SetProjection(float fov, float aspectRatio)
        {
            base.SetProjection(fov, aspectRatio);
            RecalculatePerspectiveViewMatrix();
        }

        public override void SetPosition(Vector3f position)
        {
            base.SetPosition(position);
            RecalculatePerspectiveViewMatrix();
        }

        public override void SetRotation(float radians)
        {
            base.SetRotation(radians);
            RecalculatePerspectiveViewMatrix();
        }

        public override void SetPerspective(Vector3f front, Vector3f up)
        {
            base.SetPerspective(front, up);
            _Front = front;
            _Up = up;
            RecalculatePerspectiveViewMatrix();
        }

        private void RecalculatePerspectiveViewMatrix()
        {
            View = Matrix4f.LookAt(Position, Position + _Front, _Up);
            ProjectionView = Projection * View;
        }
    }
}
