using Magma.Maths;
using QubEngine.Core.Events;

namespace QubEngine.Core.Rendering
{
    public abstract class CameraController
    {
        public Camera Camera { get; protected set; }
        protected float AspectRatio;
        protected float _ZoomLevel;
        protected float _Rotation;
        protected float _RotationSpeed;
        protected float _TranslationSpeed;
        protected Vector3f _Position;
        public float ZoomLevel { get { return _ZoomLevel; } }
        public float Rotation { get { return _Rotation; } }
        public float RotationSpeed { get { return _RotationSpeed; } }
        public float TranslationSpeed { get { return _TranslationSpeed; } protected set { _TranslationSpeed = value; } }
        public Vector3f Position { get { return _Position; } }

        protected virtual void Initialize(float aspectRatio)
        {
            _Position = new Vector3f(0.0f, 0.0f, 0.0f);
            _TranslationSpeed = 5.0f;
            _RotationSpeed = 20.0f;
            _Rotation = 0.0f;
            _ZoomLevel = 1.0f;
            AspectRatio = aspectRatio;
        }

        public abstract void OnUpdate(TimeStep timeStep);
        public abstract void SetRotationSpeed(float rotationSpeed);
        public abstract void SetTranslationSpeed(float translationSpeed);

        public virtual void SetZoom(float zoom)
        {
            Camera.SetZoom(zoom);
        }

        public virtual void SetPosition(Vector3f position)
        {
            Camera.SetPosition(position);
        }

        public virtual void SetRotation(float radians)
        {
            Camera.SetRotation(radians);
        }

        public virtual void OnEvent(Event e) { }
    }
}
