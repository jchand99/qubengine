using Magma.Logger;
using Magma.Maths;
using Magma.OpenGL;

namespace QubEngine.Core.Rendering
{
    public class Renderer2D
    {
        internal class Data
        {
            internal const uint MaxQuads = 10000;
            internal const uint MaxVertices = MaxQuads * 4;
            internal const uint MaxIndices = MaxQuads * 6;
            internal const uint MaxTextureSlots = 32; // TODO: RendererCaps

            internal VertexArray QuadVertexArray;
            internal VertexBuffer QuadVertexBuffer;
            internal IndexBuffer QuadIndexBuffer;

            // internal VertexArray ScreenVertexArray;
            // internal VertexBuffer ScreenVertexBuffer;
            internal Shader TextureShader;
            // internal Shader ScreenShader;
            internal Texture2D WhiteTexture;
            internal static readonly Vector4f WhiteColor = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

            internal int VerticesIndex { get; set; }
            internal uint IndexCount { get; set; } = 0;
            internal Vertex[] Vertices = new Vertex[MaxVertices];

            internal Texture2D[] TextureSlots = new Texture2D[MaxTextureSlots];
            internal uint TextureSlotIndex = 1; // 0 = WhiteTexture;

            internal Vector4f[] QuadVertexPositions = new Vector4f[4];
            internal Vector2f[] TexCoords = new Vector2f[4];
        }

        private static Data _Data;

        public static void Init()
        {
            _Data = new Data();
            _Data.QuadVertexArray = VertexArray.Create();
            _Data.QuadVertexArray.Bind();

            _Data.QuadVertexBuffer = VertexBuffer.Create(Data.MaxVertices * Vertex.SizeInBytes);
            _Data.QuadVertexBuffer.Layout = new BufferLayout()
            {
                { ShaderDataType.Float3, "a_Position" },
                { ShaderDataType.Float4, "a_Color" },
                { ShaderDataType.Float2, "a_TexCoords" },
                { ShaderDataType.Float, "a_TexIndex" },
                { ShaderDataType.Float, "a_TilingFactor" },
            };

            uint[] indices = new uint[Data.MaxIndices];

            uint offset = 0;
            for (int i = 0; i < Data.MaxIndices; i += 6)
            {
                indices[i + 0] = offset + 0;
                indices[i + 1] = offset + 1;
                indices[i + 2] = offset + 2;

                indices[i + 3] = offset + 2;
                indices[i + 4] = offset + 3;
                indices[i + 5] = offset + 0;
                offset += 4;
            }

            _Data.QuadIndexBuffer = IndexBuffer.Create(indices);

            _Data.QuadVertexArray.AddVertexBuffer(_Data.QuadVertexBuffer);
            _Data.QuadVertexArray.SetIndexBuffer(_Data.QuadIndexBuffer);
            _Data.QuadVertexBuffer.Unbind();
            _Data.QuadIndexBuffer.Unbind();
            _Data.QuadVertexArray.Unbind();

            int[] samplers = new int[Data.MaxTextureSlots];
            for (int i = 0; i < Data.MaxTextureSlots; i++) samplers[i] = i;

            _Data.TextureShader = Shader.Create("assets/shaders/2DTextureShader.glsl");
            _Data.TextureShader.Bind();
            _Data.TextureShader.Upload("u_Textures", samplers);
            _Data.TextureShader.Unbind();

            _Data.WhiteTexture = Texture2D.Create(1, 1);
            _Data.WhiteTexture.SetData<uint>(0xffffffff, sizeof(uint));

            _Data.TextureSlots[0] = _Data.WhiteTexture;

            _Data.QuadVertexPositions[0] = new Vector4f(-0.5f, -0.5f, 0.0f, 1.0f);
            _Data.QuadVertexPositions[1] = new Vector4f(0.5f, -0.5f, 0.0f, 1.0f);
            _Data.QuadVertexPositions[2] = new Vector4f(0.5f, 0.5f, 0.0f, 1.0f);
            _Data.QuadVertexPositions[3] = new Vector4f(-0.5f, 0.5f, 0.0f, 1.0f);

            _Data.TexCoords[0] = new Vector2f(0.0f, 0.0f);
            _Data.TexCoords[1] = new Vector2f(1.0f, 0.0f);
            _Data.TexCoords[2] = new Vector2f(1.0f, 1.0f);
            _Data.TexCoords[3] = new Vector2f(0.0f, 1.0f);
        }

        public static void Shutdown()
        {
            _Data.TextureShader.Dispose();
            _Data.QuadVertexArray.Dispose();
            _Data.QuadVertexBuffer.Dispose();
            _Data.QuadIndexBuffer.Dispose();
            _Data.TextureShader.Dispose();
        }

        public static void BeginScene(CameraController controller) => BeginScene(controller.Camera);
        public static void BeginScene(Camera camera)
        {
            var projectionView = camera.ProjectionView;

            _Data.TextureShader.Bind();
            _Data.TextureShader.Upload("u_ProjectionView", ref projectionView);
            StartBatch();
        }
        public static void EndScene()
        {
            Flush();
        }

        private static void Flush()
        {
            if (_Data.IndexCount == 0) return;
            _Data.QuadVertexBuffer.Bind();
            _Data.QuadVertexBuffer.SetData<float>(ref _Data.Vertices[0].Position.X, (uint)(_Data.Vertices.Length * Vertex.SizeInBytes));
            _Data.QuadVertexBuffer.Unbind();

            for (int i = 0; i < _Data.TextureSlotIndex; i++) _Data.TextureSlots[i].Bind((uint)i);
            _Data.TextureShader.Bind();
            _Data.QuadVertexArray.Bind();
            _Data.QuadIndexBuffer.Bind();
            PlatformCommand.DrawIndexed(_Data.QuadVertexArray, _Data.IndexCount);
            _Data.QuadIndexBuffer.Unbind();
            _Data.QuadVertexArray.Unbind();
            _Data.TextureShader.Unbind();
        }

        private static void StartBatch()
        {
            _Data.TextureSlotIndex = 1;
            _Data.VerticesIndex = -1;
            _Data.IndexCount = 0;
        }

        private static void NextBatch()
        {
            Flush();
            StartBatch();
        }

        public static void DrawQuad(Vector2f position, Vector2f size, Vector4f color)
        {
            DrawQuad(new Vector3f(position, 0.0f), size, color);
        }

        private static void DrawQuad(Vector3f position, Vector2f size, Vector4f color)
        {
            const int quadVertexCount = 4;
            const float textureIndex = 0.0f;
            const float tilingFactor = 1.0f;

            if (_Data.IndexCount >= Data.MaxIndices)
                NextBatch();

            for (int i = 0; i < quadVertexCount; i++)
            {
                Vertex vertex;
                vertex.Color = color;
                vertex.Position = position + (_Data.QuadVertexPositions[i] * new Vector3f(size, 1.0f));
                vertex.TexCoords = _Data.TexCoords[i];
                vertex.TexIndex = textureIndex;
                vertex.TilingFactor = tilingFactor;

                _Data.Vertices[++_Data.VerticesIndex] = vertex;
            }
            _Data.IndexCount += 6;
        }

        public static void DrawQuad(Vector2f position, Vector2f size, Texture2D texture, int tilingFactor = 1)
        {
            DrawQuad(new Vector3f(position, 0.0f), size, texture, tilingFactor);
        }

        public static void DrawQuad(Vector3f position, Vector2f size, Texture2D texture, int tilingFactor = 1)
        {
            if (_Data.IndexCount >= Data.MaxIndices) NextBatch();
            float textureIndex = GetTextureIndex(texture);

            if (_Data.IndexCount >= Data.MaxIndices)
                NextBatch();

            const int quadVertexCount = 4;
            for (int i = 0; i < quadVertexCount; i++)
            {
                Vertex vertex;
                vertex.Color = Data.WhiteColor;
                vertex.Position = position + (_Data.QuadVertexPositions[i] * new Vector3f(size, 1.0f));
                vertex.TexCoords = _Data.TexCoords[i];
                vertex.TexIndex = textureIndex;
                vertex.TilingFactor = tilingFactor;

                _Data.Vertices[++_Data.VerticesIndex] = vertex;
            }
            _Data.IndexCount += 6;
        }

        public static void DrawQuad(Vector3f position, Vector2f size, SubTexture2D subTexture, int tilingFactor = 1)
        {
            if (_Data.IndexCount >= Data.MaxIndices) NextBatch();
            float textureIndex = GetTextureIndex(subTexture.Texture);


            const int quadVertexCount = 4;
            for (int i = 0; i < quadVertexCount; i++)
            {
                Vertex vertex;
                vertex.Color = Data.WhiteColor;
                vertex.Position = position + (_Data.QuadVertexPositions[i] * new Vector3f(size, 1.0f));
                vertex.TexCoords = _Data.TexCoords[i];
                vertex.TexIndex = textureIndex;
                vertex.TilingFactor = tilingFactor;

                _Data.Vertices[++_Data.VerticesIndex] = vertex;
            }
            _Data.IndexCount += 6;
        }

        public static void DrawRotatedQuad(Vector2f position, Vector2f size, Texture2D texture, float radians, int tilingFactor = 1)
        {
            DrawRotatedQuad(new Vector3f(position, 0.0f), size, texture, radians, tilingFactor);
        }

        public static void DrawRotatedQuad(Vector3f position, Vector2f size, Texture2D texture, float radians, int tilingFactor = 1)
        {
            if (_Data.IndexCount >= Data.MaxIndices) NextBatch();
            float textureIndex = GetTextureIndex(texture);

            Matrix4f transform = Matrix4f.Translation(position).Rotate(radians, new Vector3f(0.0f, 0.0f, 1.0f)).Scale(new Vector3f(size, 1.0f));

            const int quadVertexCount = 4;
            for (int i = 0; i < quadVertexCount; i++)
            {
                Vertex vertex;
                vertex.Color = Data.WhiteColor;
                vertex.Position = transform * _Data.QuadVertexPositions[i];
                vertex.TexCoords = _Data.TexCoords[i];
                vertex.TexIndex = textureIndex;
                vertex.TilingFactor = tilingFactor;

                _Data.Vertices[++_Data.VerticesIndex] = vertex;
            }
            _Data.IndexCount += 6;

        }

        private static float GetTextureIndex(Texture2D texture)
        {
            float textureIndex = 0.0f;
            for (int i = 1; i < _Data.TextureSlotIndex; i++)
            {
                if (_Data.TextureSlots[i].Equals(texture))
                {
                    textureIndex = i;
                    break;
                }
            }

            if (textureIndex == 0.0f)
            {
                textureIndex = _Data.TextureSlotIndex;
                _Data.TextureSlots[_Data.TextureSlotIndex++] = texture;
            }

            return textureIndex;
        }
    }
}
