using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public struct Vertex
    {
        public Vector3f Position;
        public Vector4f Color;
        public Vector2f TexCoords;
        public float TexIndex;
        public float TilingFactor;
        public const int SizeInBytes = Vector3f.SizeInBytes + Vector2f.SizeInBytes + Vector4f.SizeInBytes + sizeof(float) * 2;
    }
}
