using System;
using System.Collections;
using System.Collections.Generic;
using Magma.OpenGL;
using QubEngine.Platform.Graphics.OpenGL;

namespace QubEngine.Core.Rendering
{
    public enum ShaderDataType
    {
        None = 0, Float, Float2, Float3, Float4, Mat3, Mat4, Int, Int2, Int3, Int4, Bool
    }

    public class BufferElement
    {
        public ShaderDataType Type { get; set; }
        public string Name { get; set; }
        public uint Size { get; set; }
        public uint Offset { get; set; }
        public bool Normalized { get; set; }

        public uint ComponentCount
        {
            get
            {
                return Type switch
                {
                    ShaderDataType.None => 0,
                    ShaderDataType.Float => 1,
                    ShaderDataType.Float2 => 2,
                    ShaderDataType.Float3 => 3,
                    ShaderDataType.Float4 => 4,
                    ShaderDataType.Mat3 => 3 * 3,
                    ShaderDataType.Mat4 => 4 * 4,
                    ShaderDataType.Int => 1,
                    ShaderDataType.Int2 => 2,
                    ShaderDataType.Int3 => 2,
                    ShaderDataType.Int4 => 3,
                    ShaderDataType.Bool => 1,
                    _ => 0
                };
            }
        }

        public VertexAttribPointerType VertexAttribType
        {
            get
            {
                return Type switch
                {
                    ShaderDataType.None => VertexAttribPointerType.Float,
                    ShaderDataType.Float => VertexAttribPointerType.Float,
                    ShaderDataType.Float2 => VertexAttribPointerType.Float,
                    ShaderDataType.Float3 => VertexAttribPointerType.Float,
                    ShaderDataType.Float4 => VertexAttribPointerType.Float,
                    ShaderDataType.Mat3 => VertexAttribPointerType.Float,
                    ShaderDataType.Mat4 => VertexAttribPointerType.Float,
                    ShaderDataType.Int => VertexAttribPointerType.Int,
                    ShaderDataType.Int2 => VertexAttribPointerType.Int,
                    ShaderDataType.Int3 => VertexAttribPointerType.Int,
                    ShaderDataType.Int4 => VertexAttribPointerType.Int,
                    ShaderDataType.Bool => VertexAttribPointerType.Int,
                    _ => 0
                };
            }
        }

        public BufferElement(ShaderDataType type, string name, bool normalized = false)
        {
            Type = type;
            Name = name;
            Size = ShaderDataTypeSize(type);
            Offset = 0;
            Normalized = normalized;
        }

        private static uint ShaderDataTypeSize(ShaderDataType type)
        {
            return type switch
            {
                ShaderDataType.None => 0,
                ShaderDataType.Float => 4,
                ShaderDataType.Float2 => 4 * 2,
                ShaderDataType.Float3 => 4 * 3,
                ShaderDataType.Float4 => 4 * 4,
                ShaderDataType.Mat3 => 4 * 3 * 3,
                ShaderDataType.Mat4 => 4 * 4 * 4,
                ShaderDataType.Int => 4,
                ShaderDataType.Int2 => 4 * 2,
                ShaderDataType.Int3 => 4 * 2,
                ShaderDataType.Int4 => 4 * 3,
                ShaderDataType.Bool => 4,
                _ => 0
            };
        }
    }

    public class BufferLayout : IEnumerable<BufferElement>
    {
        private List<BufferElement> _Elements;
        public uint Stride { get; private set; }
        public int Length { get => _Elements.Count; }

        public BufferElement this[int index]
        {
            get => _Elements[index];
        }

        public void Add(BufferElement element)
        {
            _Elements.Add(element);
            Stride += element.Size;
            element.Offset = Stride - element.Size;
        }
        public void Add(ShaderDataType type, string name, bool normalized = false)
        => Add(new BufferElement(type, name, normalized));

        public BufferLayout() : this(new List<BufferElement>()) { }
        public BufferLayout(List<BufferElement> elements)
        {
            _Elements = elements;
            CalculateOffsetsAndStride();
        }

        private void CalculateOffsetsAndStride()
        {
            uint offset = 0;
            Stride = 0;
            for (int i = 0; i < _Elements.Count; i++)
            {
                _Elements[i].Offset = offset;
                offset += _Elements[i].Size;
                Stride += _Elements[i].Size;
            }
        }

        public IEnumerator<BufferElement> GetEnumerator()
        {
            return ((IEnumerable<BufferElement>)_Elements).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_Elements).GetEnumerator();
        }
    }

    public abstract class VertexBuffer : IDisposable
    {
        public BufferLayout Layout { get; set; }
        private bool disposedValue;

        public static VertexBuffer Create(uint size)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is currently not supported!"),
                API.OpenGL => new OpenGLVertexBuffer(size),
                _ => throw new QubEngineException("Selected rendering API does not exist!")
            };
        }
        public static VertexBuffer Create(ref float[] data)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is currently not supported!"),
                API.OpenGL => new OpenGLVertexBuffer(ref data),
                _ => throw new QubEngineException("Selected rendering API does not exist!")
            };
        }

        public abstract void Bind();
        public abstract void Unbind();

        public abstract void SetData<T>(Span<T> data, uint size) where T : unmanaged;
        public abstract void SetData<T>(ref T data, uint size) where T : unmanaged;

        public abstract void Destroy();

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing) { }

                Destroy();
                disposedValue = true;
            }
        }

        ~VertexBuffer()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    public abstract class IndexBuffer : IDisposable
    {
        public BufferLayout Layout { get; set; }
        public int Count { get; protected set; }
        private bool disposedValue;

        public static IndexBuffer Create(Span<uint> data)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLIndexBuffer(data),
                _ => throw new QubEngineException("Selected rendering API does not exist!")
            };
        }

        public abstract void Bind();
        public abstract void Unbind();

        public abstract void Destroy();

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing) { }

                Destroy();
                disposedValue = true;
            }
        }

        ~IndexBuffer()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
