using System;
using Magma.Logger;
using Magma.Maths;
using QubEngine.Core.Events;

namespace QubEngine.Core.Rendering
{
    public class OrthographicCameraController : CameraController
    {
        private bool _AllowRotation;

        public OrthographicCameraController(float aspectRatio, bool rotation = false)
        {
            Initialize(aspectRatio);
            _AllowRotation = rotation;
            Camera = new OrthographicCamera(-aspectRatio * ZoomLevel, aspectRatio * ZoomLevel, -ZoomLevel, ZoomLevel, -1.0f, 1.0f);
        }

        public override void OnUpdate(TimeStep ts)
        {
            if (Input.IsKeyPressed(Key.A))
            {
                _Position.X -= MathF.Cos(Rotation.ToRadians()) * TranslationSpeed * ts;
                _Position.Y -= MathF.Sin(Rotation.ToRadians()) * TranslationSpeed * ts;
            }
            if (Input.IsKeyPressed(Key.D))
            {
                _Position.X += MathF.Cos(Rotation.ToRadians()) * TranslationSpeed * ts;
                _Position.Y += MathF.Sin(Rotation.ToRadians()) * TranslationSpeed * ts;
            }

            if (Input.IsKeyPressed(Key.W))
            {
                _Position.X += -MathF.Sin(Rotation.ToRadians()) * TranslationSpeed * ts;
                _Position.Y += MathF.Cos(Rotation.ToRadians()) * TranslationSpeed * ts;
            }
            if (Input.IsKeyPressed(Key.S))
            {
                _Position.X -= -MathF.Sin(Rotation.ToRadians()) * TranslationSpeed * ts;
                _Position.Y -= MathF.Cos(Rotation.ToRadians()) * TranslationSpeed * ts;
            }

            if (_AllowRotation)
            {
                if (Input.IsKeyPressed(Key.Q))
                    _Rotation += RotationSpeed * ts;
                if (Input.IsKeyPressed(Key.E))
                    _Rotation -= RotationSpeed * ts;

                if (Rotation > 180.0f)
                    _Rotation -= 360.0f;
                else if (Rotation <= -180.0f)
                    _Rotation += 360.0f;

                Camera.SetRotation(Rotation);
            }

            Camera.SetPosition(Position);

            _TranslationSpeed = ZoomLevel;
        }

        public override void SetZoom(float zoom)
        {
            _ZoomLevel = zoom;
            CalculateProjection(AspectRatio, zoom);
        }

        public override void SetRotationSpeed(float rotationSpeed)
        {
            _RotationSpeed = rotationSpeed;
        }

        public override void SetTranslationSpeed(float translationSpeed)
        {
            _TranslationSpeed = translationSpeed;
        }

        private void CalculateProjection(float aspectRatio, float zoom)
        {
            Camera.SetProjection(-aspectRatio * zoom, aspectRatio * zoom, -zoom, zoom);
        }

        public override void OnEvent(Event @event)
        {
            EventDispatcher ed = new EventDispatcher(@event);
            ed.Dispatch<MouseScrolledEvent>(OnMouseScrolled);
            ed.Dispatch<WindowResizeEvent>(OnWindowResize);
        }

        private bool OnWindowResize(WindowResizeEvent arg)
        {
            AspectRatio = (float)arg.Width / (float)arg.Height;
            CalculateProjection(AspectRatio, ZoomLevel);
            return false;
        }

        private bool OnMouseScrolled(MouseScrolledEvent arg)
        {
            _ZoomLevel -= arg.YOffset * 0.25f;
            _ZoomLevel = MathF.Max(_ZoomLevel, 0.25f);
            CalculateProjection(AspectRatio, _ZoomLevel);
            return false;
        }
    }
}
