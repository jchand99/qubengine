using System;
using System.Collections.Generic;

using Magma.Logger;
using Magma.Maths;
using QubEngine.Core.Utils;
using QubEngine.Platform.Graphics.OpenGL;

namespace QubEngine.Core.Rendering
{
    public abstract class Shader : IDisposable
    {
        private bool disposedValue;

        public string Name { get; protected set; }

        public static Shader Create(string directory, string filePath) => Create(IOUtil.Combine(directory, filePath));
        public static Shader Create(string path)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineCoreException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLShader(path),
                _ => throw new QubEngineCoreException("Selected rendering API does not exist!")
            };
        }
        public static Shader Create(string vertexSource, string fragmentSource, string name)
        {
            return RendererAPI.SelectedAPI switch
            {
                API.None => throw new QubEngineCoreException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLShader(vertexSource, fragmentSource, name),
                _ => throw new QubEngineCoreException("Selected rendering API does not exist!")
            };
        }

        public abstract void Bind();
        public abstract void Unbind();

        public abstract void Upload(string name, float value);
        public abstract void Upload(string name, ref Vector3f values);
        public abstract void Upload(string name, ref Vector4f values);
        public abstract void Upload(string name, int value);
        public abstract void Upload(string name, int[] values);
        public abstract void Upload(string name, ref Matrix3f matrix);
        public abstract void Upload(string name, ref Matrix4f matrix);

        protected abstract void Destroy();

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                }
                Destroy();
                disposedValue = true;
            }
        }

        ~Shader()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    public class ShaderLibrary : IDisposable
    {
        private Dictionary<string, Shader> _Shaders = new Dictionary<string, Shader>();
        private bool disposedValue;

        public Shader this[string name]
        {
            get
            {
                bool result = false;
                Shader shader = null;
                try
                {
                    result = _Shaders.TryGetValue(name, out shader);
                }
                catch (ArgumentNullException e)
                {
                    Log.Error<ShaderLibrary>(e.Message);
                }
                return shader;
            }
        }

        public void Add(Shader shader) => Add(shader.Name, shader);
        public void Add(string name, Shader shader)
        {
            if (string.IsNullOrEmpty(name) || shader == null) return;
            Log.Assert<ShaderLibrary>(!Exists(name), "Shader already exists!");
            _Shaders.Add(name, shader);
        }

        public Shader Load(string directory, string fileName, string name) => Load(IOUtil.Combine(directory, fileName), name);
        public Shader Load(string path)
        {
            Shader shader = Shader.Create(path);
            Add(shader.Name, shader);
            return shader;
        }
        public Shader Load(string path, string name)
        {
            Shader shader = Shader.Create(path);
            Add(name, shader);
            return shader;
        }

        public bool Exists(string name)
        {
            bool result = false;
            try
            {
                result = _Shaders.ContainsKey(name);
            }
            catch (ArgumentNullException e)
            {
                Log.Error<ShaderLibrary>(e.Message);
            }

            return result;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                foreach (var shader in _Shaders)
                {
                    shader.Value.Dispose();
                }

                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~ShaderLibrary()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
