using QubEngine.Platform.Graphics.OpenGL;
using QubEngine.Platform.Graphics.Vulkan;
using Magma.Maths;

namespace QubEngine.Core.Rendering
{
    public abstract class RendererAPI
    {
        public static API SelectedAPI { get; private set; } = API.OpenGL;

        public static RendererAPI Create()
        {
            return SelectedAPI switch
            {
                API.None => throw new QubEngineException($"Rendering API: ({API.None}) is not currenlty supported!"),
                API.OpenGL => new OpenGLRendererAPI(),
                API.Vulkan => new VulkanRendererAPI(),
                _ => throw new QubEngineException("Selected rendering API does not exist!")
            };
        }

        public abstract void SetViewport(int x, int y, int width, int height);
        public abstract void SetClearColor(Vector4f color);
        public abstract void Clear();
        public abstract void DrawIndexed(VertexArray vertexArray, uint indexCount);


    }

    public enum API
    {
        None = 0,
        OpenGL = 1,
        Vulkan = 2,
    }
}
