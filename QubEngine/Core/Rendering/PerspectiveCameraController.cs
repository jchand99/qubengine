
using System;
using Magma.Maths;
using QubEngine.Core.Events;

namespace QubEngine.Core.Rendering
{
    public class PerspectiveCameraController : CameraController
    {
        private Vector3f _Front = new Vector3f(0.0f, 0.0f, -1.0f);
        private Vector3f _Right;
        private Vector3f _Up;

        private float _Pitch;
        private float _Yaw;
        private float _LastX;
        private float _LastY;
        private float _MouseSensitivity;
        private bool _FirstMouse;

        public PerspectiveCameraController(float fov, float aspectRatio) : this(fov, aspectRatio, new Vector3f(0.0f, 0.0f, 0.0f)) { }
        public PerspectiveCameraController(float fov, float aspectRatio, Vector3f position)
        {
            Initialize(aspectRatio);
            Camera = new PerspectiveCamera(fov, aspectRatio, 0.1f, 1000.0f);
            SetPosition(position);
            UpdateCameraVectors();
        }

        protected override void Initialize(float aspectRatio)
        {
            base.Initialize(aspectRatio);
            _Pitch = 0.0f;
            _Yaw = -90.0f;
            _FirstMouse = true;
            _MouseSensitivity = 0.1f;
            TranslationSpeed = 10.0f;
        }

        public override void OnUpdate(TimeStep ts)
        {
            float velocity = TranslationSpeed * ts;
            if (Input.IsKeyPressed(Key.A))
                _Position -= _Right * velocity;
            if (Input.IsKeyPressed(Key.D))
                _Position += _Right * velocity;

            if (Input.IsKeyPressed(Key.W))
                _Position += _Front * velocity;
            if (Input.IsKeyPressed(Key.S))
                _Position -= _Front * velocity;

            if (Input.IsKeyPressed(Key.Space))
                _Position.Y += velocity;
            if (Input.IsKeyPressed(Key.LeftShift))
                _Position.Y -= velocity;

            Camera.SetPosition(_Position);
        }

        public override void OnEvent(Event e)
        {
            EventDispatcher ed = new EventDispatcher(e);
            ed.Dispatch<MouseMovedEvent>(OnMouseMoved);
        }

        public override void SetPosition(Vector3f position)
        {
            base.SetPosition(position);
            _Position = position;
        }
        public override void SetTranslationSpeed(float translationSpeed) => _TranslationSpeed = translationSpeed;
        public override void SetRotationSpeed(float rotationSpeed) => _RotationSpeed = rotationSpeed;

        private bool OnMouseMoved(MouseMovedEvent arg)
        {
            if (_FirstMouse)
            {
                _LastX = arg.X;
                _LastY = arg.Y;
                _FirstMouse = false;
            }

            float xOffset = arg.X - _LastX;
            float yOffset = _LastY - arg.Y;

            _LastX = arg.X;
            _LastY = arg.Y;

            ProcessMouseMovement(xOffset, yOffset);
            return false;
        }

        private void ProcessMouseMovement(float xOffset, float yOffset, bool constrainPitch = true)
        {
            xOffset *= _MouseSensitivity;
            yOffset *= _MouseSensitivity;

            _Yaw += xOffset;
            _Pitch += yOffset;

            if (constrainPitch)
            {
                if (_Pitch > 89.0f) _Pitch = 89.0f;
                if (_Pitch < -89.0f) _Pitch = -89.0f;
            }
            UpdateCameraVectors();
        }

        private void UpdateCameraVectors()
        {
            Vector3f front;
            Vector3f pointing;

            var yawR = _Yaw.ToRadians();
            var pitchR = _Pitch.ToRadians();
            pointing.X = MathF.Cos(yawR) * MathF.Cos(pitchR);
            pointing.Y = MathF.Sin(pitchR);
            pointing.Z = MathF.Sin(yawR) * MathF.Cos(pitchR);

            front.X = MathF.Cos(yawR);
            front.Y = 0.0f;
            front.Z = MathF.Sin(yawR);

            _Front = Vector3f.Normalize(front);
            _Right = Vector3f.Normalize(Vector3f.Cross(_Front, Vector3f.Up));
            _Up = Vector3f.Normalize(Vector3f.Cross(_Right, _Front));

            Camera.SetPerspective(Vector3f.Normalize(pointing), _Up);
        }
    }
}
