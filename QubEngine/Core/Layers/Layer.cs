using QubEngine.Core.Events;

namespace QubEngine.Core.Layers
{
    public abstract class Layer
    {
        public string Name { get; protected set; }

        protected Layer(string name) { Name = name; }

        public abstract void OnAttach();
        public abstract void OnDetach();
        public abstract void OnUpdate(TimeStep timeStep);
        public abstract void OnEvent(Event @event);
    }
}
