using System.Collections.Generic;

namespace QubEngine.Core.Layers
{
    public class LayerStack
    {
        private List<Layer> _Layers;
        public int Count { get { return _Layers.Count; } private set { Count = value; } }

        public Layer this[int i]
        {
            get { return _Layers[i]; }
        }

        public LayerStack()
        {
            _Layers = new List<Layer>();
        }

        public void CleanUp()
        {
            for (int i = 0; i < Count; i++)
            {
                _Layers[i].OnDetach();
            }
        }

        public void PushLayer(Layer layer) => _Layers.Add(layer);
        public void PushOverlay(Layer layer) => _Layers.Add(layer);

        public void PopLayer(Layer layer)
        {
            var layerToRemove = _Layers.Find(x => x.Name == layer.Name);
            if (layerToRemove != null)
            {
                layerToRemove.OnDetach();
                _Layers.Remove(layerToRemove);
            }
        }

        public void PopOverlay(Layer layer)
        {
            var overlayToRemove = _Layers.Find(x => x.Name == layer.Name);
            if (overlayToRemove != null)
            {
                overlayToRemove.OnDetach();
                _Layers.Remove(overlayToRemove);
            }
        }
    }
}
