using System;
using System.Collections;
using System.Runtime.Serialization;

namespace QubEngine
{
    internal class QubEngineException : Exception
    {
        internal QubEngineException() { }
        internal QubEngineException(string message) : base(message) { }
        internal QubEngineException(object thrower, string message) : base($"{nameof(thrower)} => {message}") { }
        internal QubEngineException(string message, Exception innerException) : base(message, innerException) { }
        internal QubEngineException(object thrower, string message, Exception innerException) : base($"{nameof(thrower)} => {message}", innerException) { }
        protected QubEngineException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public override IDictionary Data => base.Data;

        public override string HelpLink { get => base.HelpLink; set => base.HelpLink = value; }

        public override string Message => base.Message;

        public override string Source { get => base.Source; set => base.Source = value; }

        public override string StackTrace => base.StackTrace;

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Exception GetBaseException()
        {
            return base.GetBaseException();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
