using System;
using Magma.Vulkan;

using QubEngine.Core.Rendering;

namespace QubEngine.Platform.Graphics.Vulkan
{
    public class VulkanFramebuffer : Framebuffer
    {
        private VkFramebuffer _Framebuffer;
        private VkImageView _ImageView;
        private VkRenderPass _RenderPass;

        public VulkanFramebuffer(VkRenderPass renderpass, VkImageView imageView, uint width, uint height)
        {
            _ImageView = imageView;
            _RenderPass = renderpass;

            FramebufferCreateInfo createInfo = new FramebufferCreateInfo
            {
                Attachments = new VkImageView[] { imageView },
                RenderPass = renderpass,
                Height = (int)height,
                Width = (int)width,
                Layers = 1,
            };

            _Framebuffer = new VkFramebuffer(VulkanContext.Device, createInfo);
        }

        public override void Bind()
        {
            throw new NotImplementedException();
        }

        public override uint GetColorAttachmentRendererID()
        {
            throw new NotImplementedException();
        }

        public override void Resize(uint width, uint height)
        {
            FramebufferCreateInfo createInfo = new FramebufferCreateInfo
            {
                Attachments = new VkImageView[] { _ImageView },
                RenderPass = _RenderPass,
                Height = (int)height,
                Width = (int)width,
            };

            _Framebuffer = new VkFramebuffer(VulkanContext.Device, createInfo);
        }

        public override void Unbind()
        {
            throw new NotImplementedException();
        }

        protected override void Destroy()
        {
            throw new NotImplementedException();
        }
    }
}
