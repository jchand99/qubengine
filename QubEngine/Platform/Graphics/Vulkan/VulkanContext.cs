using System;
using System.Collections.Generic;

using QubEngine.Core.Rendering;

using Magma.GLFW;
using Magma.Vulkan;
using Magma.Vulkan.EXT;
using Magma.Vulkan.KHR;
using Magma.Logger;
using System.Linq;

namespace QubEngine.Platform.Graphics.Vulkan
{
    internal struct QueueFamilyIndices
    {
        internal uint GraphicsFamilyIndex;
        internal uint PresentFamilyIndex;

        internal QueueFamilyIndices(uint graphicsFamilyIndex = uint.MaxValue, uint presentFamilyIndex = uint.MaxValue)
        {
            GraphicsFamilyIndex = graphicsFamilyIndex;
            PresentFamilyIndex = presentFamilyIndex;
        }

        internal bool IsComplete => GraphicsFamilyIndex != uint.MaxValue && PresentFamilyIndex != uint.MaxValue;
    }

    internal struct SwapchainSupportDetails
    {
        internal SurfaceCapabilitiesKhr Capabilities;
        internal SurfaceFormatKhr[] Formats;
        internal PresentModeKhr[] PresentModes;
    }

    public class VulkanContext : GraphicsContext
    {
        public static VkInstance Instance { get; private set; }
        public static VkPhysicalDevice PhysicalDevice { get; private set; }
        public static VkDevice Device { get; private set; }
        public static VkSurfaceKhr Surface { get; private set; }

        private static VkQueue _GraphicsQueue;
        public static VkQueue GraphicsQueue { get { return _GraphicsQueue; } }
        private static VkQueue _PresentQueue;
        public static VkQueue PresentQueue { get { return _PresentQueue; } }

        private static VkDebugUtilsMessengerExt _DebugUtilsMessenger;

        public VulkanContext(IntPtr windowHandle)
        {
            _CreateInstance();
            _CreateDebugUtils();
            _CreateSurface(windowHandle);
            _PickPhysicalDevice();
            _CreateDevice();
        }

        private static void _CreateInstance()
        {
            ApplicationInfo applicationInfo = new ApplicationInfo
            {
                ApiVersion = new VkVersion(1, 2, 172),
                ApplicationName = "QubEngine",
                ApplicationVersion = new VkVersion(1, 0, 0),
                EngineName = "QubEngine",
                EngineVersion = new VkVersion(1, 0, 0),
            };

            List<string> extensions = new List<string>(Glfw.GetRequiredInstanceExtensions(out int extensionCount));
            extensions.Add("VK_KHR_surface");

#if DEBUG
            extensions.Add("VK_EXT_debug_report");
            extensions.Add("VK_EXT_debug_utils");
#endif

            InstanceCreateInfo instanceCreateInfo = new InstanceCreateInfo
            {
                ApplicationInfo = applicationInfo,
                EnabledExtensionNames = extensions.ToArray(),
#if DEBUG
                EnabledLayerNames = new string[] { "VK_LAYER_KHRONOS_validation" },
#endif
            };

            Instance = new VkInstance(instanceCreateInfo);
        }

#if DEBUG
        private static void _CreateDebugUtils()
        {
            DebugUtilsMessengerCreateInfoExt debugUtilsMessengerCreateInfoExt = new DebugUtilsMessengerCreateInfoExt
            {
                MessageSeverity = DebugUtilsMessageSeveritiesExt.Verbose | DebugUtilsMessageSeveritiesExt.Info | DebugUtilsMessageSeveritiesExt.Warning | DebugUtilsMessageSeveritiesExt.Error,
                MessageTypes = DebugUtilsMessageTypesExt.General | DebugUtilsMessageTypesExt.Validation | DebugUtilsMessageTypesExt.Performance,
                UserCallback = _DebugCallback,
            };

            _DebugUtilsMessenger = new VkDebugUtilsMessengerExt(Instance, debugUtilsMessengerCreateInfoExt);
        }

        private static bool _DebugCallback(DebugUtilsMessageSeveritiesExt messageSeveritiesExt, DebugUtilsMessageTypesExt messageTypesExt, DebugUtilsMessengerCallbackDataExt callbackData, IntPtr userData)
        {
            switch (messageSeveritiesExt)
            {
                case DebugUtilsMessageSeveritiesExt.Info:
                case DebugUtilsMessageSeveritiesExt.Verbose:
                    Log.Info<VulkanContext>(callbackData.Message);
                    break;
                case DebugUtilsMessageSeveritiesExt.Warning:
                    Log.Warning<VulkanContext>(callbackData.Message);
                    break;
                case DebugUtilsMessageSeveritiesExt.Error:
                    Log.Error<VulkanContext>(callbackData.Message);
                    break;
                default:
                    Log.Fatal<VulkanContext>("This should never ben seen!");
                    break;
            }

            return false;
        }
#endif

        private static void _CreateSurface(IntPtr window)
        {
            if ((VkResult)Glfw.CreateWindowSurface(Instance, window, IntPtr.Zero, out var handle) != VkResult.Success)
            {
                _DebugUtilsMessenger.Dispose();
                Instance.Dispose();
                throw new QubEnginePlatformException("Glfw failed to create surface!");
            }

            Surface = VkSurfaceKhr.CreateFromGlfwSurfacePointer(Instance, handle);
        }

        private static void _PickPhysicalDevice()
        {
            Instance.EnumeratePhysicalDevices(out VkPhysicalDevice[] physicalDevices);
            if (physicalDevices.Length == 0) throw new QubEnginePlatformException("Failed to find graphics device for vulkan!");
            if (physicalDevices.Length == 1)
            {
                PhysicalDevice = physicalDevices[0];
                return;
            }

            SortedDictionary<int, VkPhysicalDevice> suitableDevices = new SortedDictionary<int, VkPhysicalDevice>();

            for (int i = 0; i < physicalDevices.Length; i++)
            {
                int score = _RatePhysicalDevice(physicalDevices[i]);
                suitableDevices.Add(score, physicalDevices[i]);
            }

            var last = suitableDevices.Last();
            if (last.Key > 0) PhysicalDevice = last.Value;
            if (PhysicalDevice == null) throw new QubEnginePlatformException("Failed to find suitable GPU!");

            PhysicalDevice.GetProperties(out var properties);
            Log.Info<VulkanContext>($"Vulkan Info:\n  Vendor: {properties.VendorID}\n  Name: {properties.DeviceName}\n  Driver Version: {properties.DriverVersion}");

        }

        private static int _RatePhysicalDevice(VkPhysicalDevice physicalDevice)
        {
            physicalDevice.GetProperties(out var properties);
            physicalDevice.GetFeatures(out var features);

            QueueFamilyIndices indices = FindQueueFamilies(physicalDevice);
            bool extensionsSupported = CheckPhysicalDeviceExtensionSupport(physicalDevice);
            if (!indices.IsComplete || !extensionsSupported) return 0;

            SwapchainSupportDetails swapchainSupportDetails = QuerySwapchainSupport(physicalDevice);
            bool swapchainAdequite = swapchainSupportDetails.Formats.Length != 0 && swapchainSupportDetails.PresentModes.Length != 0;
            if (!swapchainAdequite) return 0;

            int score = 0;
            if (!features.GeometryShader) return 0;

            if (properties.DeviceType == PhysicalDeviceType.DiscreteGpu) score += 100;

            score += properties.Limits.MaxImageDimension2D;

            return score;
        }

        private static QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice device)
        {
            QueueFamilyIndices result = new QueueFamilyIndices();
            device.GetQueueFamilyProperties(out var queueFamilyProperties);

            for (int i = 0; i < queueFamilyProperties.Length; i++)
            {
                if (queueFamilyProperties[i].QueueCount > 0 && queueFamilyProperties[i].QueueFlags.HasFlag(QueueFlags.Graphics)) result.GraphicsFamilyIndex = (uint)i;

                PhysicalDevice.GetSurfaceSupportKhr(result.GraphicsFamilyIndex, Surface, out bool supported);
                if (queueFamilyProperties[i].QueueCount > 0 && supported) result.PresentFamilyIndex = (uint)i;

                if (result.IsComplete) break;
            }

            return result;
        }

        private static bool CheckPhysicalDeviceExtensionSupport(VkPhysicalDevice physicalDevice)
        {
            physicalDevice.EnumerateDeviceExtensionProperties(null, out var extensionProperties);
            for (int i = 0; i < extensionProperties.Length; i++)
            {
                if (extensionProperties[i].ExtensionName.Equals("VK_KHR_swapchain")) return true;
            }

            return false;
        }

        private static SwapchainSupportDetails QuerySwapchainSupport(VkPhysicalDevice physicalDevice)
        {
            SwapchainSupportDetails details = default;
            physicalDevice.GetSurfaceCapabilitiesKhr(Surface, out details.Capabilities);
            physicalDevice.GetSurfaceFormatsKhr(Surface, out details.Formats);
            physicalDevice.GetPresentModesKhr(Surface, out details.PresentModes);
            return details;
        }

        private static void _CreateDevice()
        {
            QueueFamilyIndices indices = FindQueueFamilies(PhysicalDevice);

            PhysicalDevice.GetFeatures(out var features);
            DeviceCreateInfo deviceCreateInfo = new DeviceCreateInfo
            {
                EnabledExtensionNames = new string[] { "VK_KHR_swapchain" },
                EnabledFeatures = features,
                QueueCreateInfos = new DeviceQueueCreateInfo[] { new DeviceQueueCreateInfo(indices.GraphicsFamilyIndex, 1, new float[] { 1.0f }) }
            };

            Device = new VkDevice(PhysicalDevice, deviceCreateInfo);

            Device.GetQueue(indices.GraphicsFamilyIndex, 0, out _GraphicsQueue);
            Device.GetQueue(indices.PresentFamilyIndex, 0, out _PresentQueue);
        }

        public override void SwapBuffers()
        {
            throw new NotImplementedException();
        }
    }
}
