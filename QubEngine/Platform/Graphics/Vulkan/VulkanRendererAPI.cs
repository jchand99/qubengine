using System;
using Magma.Maths;
using Magma.Vulkan;

using QubEngine.Core.Rendering;

namespace QubEngine.Platform.Graphics.Vulkan
{
    public class VulkanRendererAPI : RendererAPI
    {
        public override void Clear()
        {
            throw new NotImplementedException();
        }

        public override void DrawIndexed(VertexArray vertexArray, uint indexCount)
        {
            throw new NotImplementedException();
        }

        public override void SetClearColor(Vector4f color)
        {
            throw new NotImplementedException();
        }

        public override void SetViewport(int x, int y, int width, int height)
        {
            throw new NotImplementedException();
        }
    }
}