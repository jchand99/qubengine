using System;
using Magma.Vulkan;
using QubEngine.Core.Rendering;

namespace QubEngine.Platform.Graphics.Vulkan
{
    public class VulkanVertexBuffer : VertexBuffer
    {
        private VkBuffer _BufferHandle;
        private VkDeviceMemory _BufferMemory;

        public VulkanVertexBuffer(uint size)
        {
            CreateBuffer((int)size, BufferUsages.TransferDst | BufferUsages.VertexBuffer, MemoryProperties.DeviceLocal, out _BufferHandle, out _BufferMemory);
        }

        public VulkanVertexBuffer(ref float[] data)
        {
            CreateBuffer(sizeof(float) * data.Length, BufferUsages.TransferDst | BufferUsages.VertexBuffer, MemoryProperties.DeviceLocal, out _BufferHandle, out _BufferMemory);
        }

        private void CreateBuffer(int size, BufferUsages usages, MemoryProperties properties, out VkBuffer buffer, out VkDeviceMemory deviceMemory)
        {
            BufferCreateInfo bufferCreateInfo = new BufferCreateInfo
            {
                Size = size,
                Usage = usages,
                SharingMode = SharingMode.Exclusive,
            };

            buffer = new VkBuffer(VulkanContext.Device, bufferCreateInfo);

            buffer.GetMemoryRequirements(out MemoryRequirements memoryRequirements);

            // Find memory type
            uint memoryType = 0;
            uint typeFilter = memoryRequirements.MemoryTypeBits;
            VulkanContext.PhysicalDevice.GetMemoryProperties(out var deviceMemoryProperties);
            for (int i = 0; i < deviceMemoryProperties.MemoryTypes.Length; i++)
            {
                if ((typeFilter & (1 << i)) > 0 && (deviceMemoryProperties.MemoryTypes[i].PropertyFlags.HasFlag(properties)))
                {
                    memoryType = (uint)i;
                    break;
                }
            }

            MemoryAllocateInfo allocateInfo = new MemoryAllocateInfo
            {
                AllocationSize = memoryRequirements.Size,
                MemoryTypeIndex = memoryType,
            };

            deviceMemory = new VkDeviceMemory(VulkanContext.Device, allocateInfo);
            buffer.BindMemory(deviceMemory, 0);
        }

        private void CopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, int size)
        {
            CommandBufferAllocateInfo allocateInfo = new CommandBufferAllocateInfo
            {
                CommandBufferCount = 1,
                Level = CommandBufferLevel.Primary
            };

            // _CommandPool.AllocateBuffers(allocateInfo, out var commandBuffers);

            // commandBuffers[0].BeginCommandBuffer(new CommandBufferBeginInfo(CommandBufferUsages.OneTimeSubmit));
            // commandBuffers[0].CmdCopyBuffer(srcBuffer, dstBuffer, new BufferCopy[] { new BufferCopy(size) });
            // commandBuffers[0].EndCommandBuffer();

            // DeviceQueueSubmitInfo submitInfo = new DeviceQueueSubmitInfo
            // {
            //     CommandBuffers = commandBuffers,
            // };

            // VulkanContext.GraphicsQueue.Submit(submitInfo, null);
            // VulkanContext.GraphicsQueue.WaitIdle();

            // _CommandPool.FreeBuffers(commandBuffers);
        }

        public override void Bind()
        {
            throw new NotImplementedException();
        }

        public override void Destroy()
        {
            throw new NotImplementedException();
        }

        public override unsafe void SetData<T>(Span<T> data, uint size)
        {
            VkBuffer stagingBuffer;
            VkDeviceMemory stagingBufferMemory;
            CreateBuffer((int)size, BufferUsages.TransferSrc, MemoryProperties.HostVisible | MemoryProperties.HostCoherent, out stagingBuffer, out stagingBufferMemory);

            fixed (T* ptr = &data[0])
            {
                stagingBufferMemory.Map(0, size, MemoryMapFlags.None, out IntPtr bufferData);
                Buffer.MemoryCopy(ptr, bufferData.ToPointer(), size, size);
                stagingBufferMemory.Unmap();
            }

            CopyBuffer(stagingBuffer, _BufferHandle, (int)size);

            stagingBuffer.Dispose();
            stagingBufferMemory.Dispose();
        }

        public override unsafe void SetData<T>(ref T data, uint size)
        {
            VkBuffer stagingBuffer;
            VkDeviceMemory stagingBufferMemory;
            CreateBuffer((int)size, BufferUsages.TransferSrc, MemoryProperties.HostVisible | MemoryProperties.HostCoherent, out stagingBuffer, out stagingBufferMemory);

            fixed (T* ptr = &data)
            {
                stagingBufferMemory.Map(0, size, MemoryMapFlags.None, out IntPtr bufferData);
                Buffer.MemoryCopy(ptr, bufferData.ToPointer(), size, size);
                stagingBufferMemory.Unmap();
            }

            CopyBuffer(stagingBuffer, _BufferHandle, (int)size);

            stagingBuffer.Dispose();
            stagingBufferMemory.Dispose();
        }

        public override void Unbind()
        {
            throw new NotImplementedException();
        }
    }
}
