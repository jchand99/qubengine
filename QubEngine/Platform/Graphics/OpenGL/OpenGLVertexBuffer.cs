using System;
using Magma.OpenGL;
using QubEngine.Core.Rendering;

namespace QubEngine.Platform.Graphics.OpenGL
{
    public class OpenGLVertexBuffer : VertexBuffer
    {
        private uint _BufferID;
        public bool IsBound { get; private set; }

        public OpenGLVertexBuffer(uint size)
        {
            Gl.CreateBuffer(out _BufferID);

            Gl.BindBuffer(BufferTarget.ArrayBuffer, _BufferID);
            Gl.BufferData<float>(BufferTarget.ArrayBuffer, (int)size, null, BufferUsageHint.StaticDraw);
            IsBound = true;
        }

        public OpenGLVertexBuffer(ref float[] data)
        {
            Gl.CreateBuffer(out _BufferID);

            Gl.BindBuffer(BufferTarget.ArrayBuffer, _BufferID);
            Gl.BufferData<float>(BufferTarget.ArrayBuffer, sizeof(float) * data.Length, data, BufferUsageHint.StaticDraw);
            IsBound = true;
        }

        public override void Bind()
        {
            if (!IsBound)
            {
                Gl.BindBuffer(BufferTarget.ArrayBuffer, _BufferID);
                IsBound = true;
            }
        }

        public override void Unbind()
        {
            if (IsBound)
            {
                Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);
                IsBound = false;
            }
        }

        public override void SetData<T>(Span<T> data, uint size)
        {
            if (!IsBound) throw new QubEnginePlatformException("Vertex buffer must be bound to set data!");
            Gl.BufferSubData<T>(BufferTarget.ArrayBuffer, 0, (int)size, data);
        }

        public override void SetData<T>(ref T data, uint size)
        {
            if (!IsBound) throw new QubEnginePlatformException("Vertex buffer must be bound to set data!");
            Gl.BufferSubData<T>(BufferTarget.ArrayBuffer, 0, (int)size, ref data);
        }

        public override void Destroy()
        {
            Gl.DeleteBuffer(_BufferID);
        }
    }
}
