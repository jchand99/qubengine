using System;
using System.IO;

using StbiSharp;

using Magma.Logger;
using Magma.OpenGL;

using QubEngine.Core.Rendering;
using QubEngine.Core.Utils;

namespace QubEngine.Platform.Graphics.OpenGL
{
    public class OpenGLTexture2D : Texture2D
    {
        public bool IsBound { get; private set; }
        public uint Slot { get; private set; }
        private PixelInternalFormat _InternalFormat;
        private PixelFormat _PixelFormat;

        public OpenGLTexture2D(int width, int height)
        {
            Width = width;
            Height = height;

            _InternalFormat = PixelInternalFormat.Rgba8;
            _PixelFormat = PixelFormat.Rgba;

            GenerateTexture(width, height);

            IsBound = false;
            Slot = 0;
        }

        public OpenGLTexture2D(string path)
        {
            ReadOnlySpan<byte> data = default;
            try
            {
                data = IOUtil.ReadAllBytes(path);
            }
            catch (IOException e)
            {
                Log.Error<OpenGLTexture2D>(e.Message);
            }

            if (!data.IsEmpty)
            {
                Stbi.SetFlipVerticallyOnLoad(true);
                StbiImage image = Stbi.LoadFromMemory(data, 0);
                Width = image.Width;
                Height = image.Height;

                switch (image.NumChannels)
                {
                    case 4:
                        _InternalFormat = PixelInternalFormat.Rgba8;
                        _PixelFormat = PixelFormat.Rgba;
                        break;
                    case 3:
                        _InternalFormat = PixelInternalFormat.Rgb8;
                        _PixelFormat = PixelFormat.Rgb;
                        break;
                    default:
                        throw new QubEngineException($"Image format with {image.NumChannels} is not supported!");
                }

                GenerateTexture(image.Width, image.Height);
                Gl.TextureSubImage2D<byte>(TextureID, 0, 0, 0, image.Width, image.Height, _PixelFormat, PixelType.UnsignedByte, image.Data);
                Gl.GenerateMipmap(GenerateMipmapTarget.Texture2D);

                image.Dispose();
            }
        }

        private void GenerateTexture(int width, int height)
        {
            Gl.CreateTexture(TextureTarget.Texture2D, out _TextureID);
            Gl.TextureStorage2D(TextureID, 1, _InternalFormat, width, height);

            Gl.TextureParameteri(TextureID, TextureParameterName.TextureMinFilter, TextureParameter.Linear);
            Gl.TextureParameteri(TextureID, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);

            Gl.TextureParameteri(TextureID, TextureParameterName.TextureWrapS, TextureParameter.Repeat);
            Gl.TextureParameteri(TextureID, TextureParameterName.TextureWrapT, TextureParameter.Repeat);
        }

        public override void SetData<T>(Span<T> data)
        {
            AssertSizeEquality(data.Length);
            Gl.TextureSubImage2D<T>(TextureID, 0, 0, 0, Width, Height, _PixelFormat, PixelType.UnsignedByte, data);
            Gl.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        }
        public override void SetData<T>(ReadOnlySpan<T> data)
        {
            AssertSizeEquality(data.Length);
            Gl.TextureSubImage2D<T>(TextureID, 0, 0, 0, Width, Height, _PixelFormat, PixelType.UnsignedByte, data);
            Gl.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        }

        public override void SetData<T>(T data, int size)
        {
            AssertSizeEquality(size);
            Gl.TextureSubImage2D<T>(TextureID, 0, 0, 0, Width, Height, _PixelFormat, PixelType.UnsignedByte, data);
            Gl.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        }

        public override void Bind(uint slot = 0)
        {
            if (!IsBound)
            {
                Slot = slot;
                Gl.BindTextureUnit(slot, TextureID);
                IsBound = true;
            }
        }
        public override void Unbind(uint slot = 0)
        {
            if (IsBound)
            {
                Gl.BindTextureUnit(slot, 0);
                IsBound = false;
            }
        }

        private void AssertSizeEquality(int length)
        {
            int bytesPerPixel = _PixelFormat == PixelFormat.Rgba ? 4 : 3;
            Log.Assert<OpenGLTexture2D>(length == Width * Height * bytesPerPixel, "Data must be entire image!");
        }

        protected override void Destroy()
        {
            Gl.DeleteTexture(TextureID);
        }

        public override bool Equals(object obj)
        {
            return obj is OpenGLTexture2D d &&
                   _TextureID == d._TextureID &&
                   TextureID == d.TextureID;
        }

        public override int GetHashCode()
        {
            return _TextureID.GetHashCode() * _InternalFormat.GetHashCode();
        }
    }
}
