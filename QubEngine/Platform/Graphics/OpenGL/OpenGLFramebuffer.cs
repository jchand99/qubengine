using System;
using Magma.Logger;
using Magma.OpenGL;
using QubEngine.Core.Rendering;

namespace QubEngine.Platform.Graphics.OpenGL
{
    public class OpenGLFramebuffer : Framebuffer
    {
        public bool IsBound { get; private set; } = false;

        private uint _ID;
        private uint _ColorAttachment;
        private uint _DepthAttachment;
        private const uint _MaxFrameBufferSize = 8192;

        public OpenGLFramebuffer(FramebufferSpecification specification)
        {
            _Specification = specification;
            Invalidate();
        }

        private void Invalidate()
        {
            if (_ID != 0)
            {
                Gl.DeleteFramebuffer(_ID);
                Gl.DeleteTexture(_ColorAttachment);
                Gl.DeleteTexture(_DepthAttachment);
            }

            Gl.CreateFramebuffer(out _ID);
            Gl.BindFramebuffer(FramebufferTarget.Framebuffer, _ID);

            Gl.CreateTexture(TextureTarget.Texture2D, out _ColorAttachment);
            Gl.BindTexture(TextureTarget.Texture2D, _ColorAttachment);
            Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, (int)Specification.Width, (int)Specification.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
            Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Linear);
            Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Linear);

            Gl.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, _ColorAttachment, 0);

            Gl.CreateTexture(TextureTarget.Texture2D, out _DepthAttachment);
            Gl.BindTexture(TextureTarget.Texture2D, _DepthAttachment);
            Gl.TexStorage2D(TextureTarget.Texture2D, 1, PixelInternalFormat.Depth24Stencil8, (int)Specification.Width, (int)Specification.Height);
            Gl.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthStencilAttachment, TextureTarget.Texture2D, _DepthAttachment, 0);

            FramebufferErrorCode err = Gl.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            if (err != FramebufferErrorCode.FramebufferComplete) throw new QubEnginePlatformException($"Framebuffer error: {err}");

            Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public override void Bind()
        {
            if (!IsBound)
            {
                Gl.BindFramebuffer(FramebufferTarget.Framebuffer, _ID);
                Gl.Viewport(0, 0, (int)Specification.Width, (int)Specification.Height);
                IsBound = true;
            }
        }

        public override void Unbind()
        {
            if (IsBound)
            {
                Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                IsBound = false;
            }
        }

        public override void Resize(uint width, uint height)
        {
            if (width == 0 || height == 0 || width > _MaxFrameBufferSize || height > _MaxFrameBufferSize)
            {
                Log.Warning<OpenGLFramebuffer>($"Attempted to resize framebuffer to {width}, {height}.");
                return;
            }

            _Specification.Width = width;
            _Specification.Height = height;

            Invalidate();
        }

        public override uint GetColorAttachmentRendererID() => _ColorAttachment;

        protected override void Destroy()
        {
            Gl.DeleteFramebuffer(_ID);
            Gl.DeleteTexture(_ColorAttachment);
            Gl.DeleteTexture(_DepthAttachment);
        }
    }
}
