using System;
using System.Collections.Generic;
using System.IO;
using Magma.Logger;
using Magma.Maths;
using Magma.OpenGL;

using QubEngine.Core.Rendering;
using QubEngine.Core.Utils;

namespace QubEngine.Platform.Graphics.OpenGL
{
    public class OpenGLShader : Shader
    {
        private uint _ProgramID;
        public bool IsBound { get; private set; }

        public OpenGLShader(string path)
        {
            ReadOnlySpan<char> source = default;
            try
            {
                source = IOUtil.ReadAllText(path);
            }
            catch (IOException e)
            {
                Log.Error<OpenGLShader>(e.Message);
            }

            if (!source.IsEmpty)
            {
                var shaderSources = PreProcess(source);
                Compile(shaderSources);

                int start = path.LastIndexOfAny(new char[] { '/', '\\' });
                start = start == -1 ? 0 : ++start;
                int end = path.IndexOf('.');
                Name = end == -1 ? path.Substring(start) : path.Substring(start, end - start);

                IsBound = false;
            }
        }

        public OpenGLShader(string vertexSource, string fragmentSource, string name)
        {
            Compile(new Dictionary<ShaderType, string>()
            {
                { ShaderType.VertexShader, vertexSource },
                { ShaderType.FragmentShader, fragmentSource}
            });

            Name = name;
            IsBound = false;
        }

        private Dictionary<ShaderType, string> PreProcess(ReadOnlySpan<char> source)
        {
            Dictionary<ShaderType, string> shaderSources = new Dictionary<ShaderType, string>();

            string token = "#type";
            int tokenTypeLength = token.Length;
            int pos = source.IndexOf(token, StringComparison.OrdinalIgnoreCase);
            while (pos != -1)
            {
                int eol = source.IndexOf("\n", StringComparison.Ordinal);
                Log.Assert<OpenGLShader>(eol != -1, "Syntax error!");
                int begin = pos + tokenTypeLength + 1;
                ShaderType type = ShaderType.None;
                try
                {
                    type = ShaderTypeFromSpan(source.Slice(begin, eol - begin));
                }
                catch (QubEngineShaderException e)
                {
                    Log.Error<OpenGLShader>(e.Message);
                    source = source.Slice(++eol);
                    pos = source.LastIndexOf(token, StringComparison.OrdinalIgnoreCase);
                    break;
                }

                source = source.Slice(++eol);
                pos = source.LastIndexOf(token, StringComparison.OrdinalIgnoreCase);
                ReadOnlySpan<char> shader;
                if (pos == -1) shader = source.Slice(0);
                else
                {
                    shader = source.Slice(0, pos);
                    source = source.Slice(pos);
                    pos = source.IndexOf(token, StringComparison.Ordinal);
                }

                shaderSources.Add(type, shader.ToString());
            }
            return shaderSources;
        }

        private ShaderType ShaderTypeFromSpan(ReadOnlySpan<char> type)
        {
            if (type.Equals("vertex".AsSpan(), StringComparison.Ordinal)) return ShaderType.VertexShader;
            if (type.Equals("fragment".AsSpan(), StringComparison.Ordinal)
            || type.Equals("pixel".AsSpan(), StringComparison.Ordinal)) return ShaderType.FragmentShader;

            throw new QubEngineShaderException("Unknown shader type!");
        }

        private void Compile(Dictionary<ShaderType, string> shaderSources)
        {
            uint program = Gl.CreateProgram();
            uint[] shaderIDs = new uint[shaderSources.Count];
            int index = 0;
            foreach (var entry in shaderSources)
            {
                ShaderType type = entry.Key;
                string source = entry.Value;

                uint shader = Gl.CreateShader(type);
                Gl.ShaderSource(shader, source, null);
                Gl.CompileShader(shader);

                Gl.GetShaderiv(shader, ShaderParameter.CompileStatus, out var compiled);
                if (compiled == 0)
                {
                    Gl.GetShaderiv(shader, ShaderParameter.InfoLogLength, out var maxLength);
                    Gl.GetShaderInfoLog(shader, maxLength, out var length, out var infoLog);
                    Gl.DeleteShader(shader);
                    Log.Error<OpenGLShader>(infoLog);
                    break;
                }

                Gl.AttachShader(program, shader);
                shaderIDs[index++] = shader;
            }

            Gl.LinkProgram(program);

            Gl.GetProgramiv(program, ProgramParameter.LinkStatus, out var linked);
            if (linked == 0)
            {
                Gl.GetProgramiv(program, ProgramParameter.InfoLogLength, out var maxLength);
                Gl.GetProgramInfoLog(program, maxLength, out var length, out var infoLog);

                Gl.DeleteProgram(program);
                for (int i = 0; i < shaderIDs.Length; i++)
                {
                    Gl.DeleteShader(shaderIDs[i]);
                }

                Log.Error<OpenGLShader>(infoLog);
            }

            _ProgramID = program;
            for (int i = 0; i < shaderIDs.Length; i++)
            {
                Gl.DetachShader(program, shaderIDs[i]);
                Gl.DeleteShader(shaderIDs[i]);
            }
        }

        public override void Bind()
        {
            if (!IsBound)
            {
                Gl.UseProgram(_ProgramID);
                IsBound = true;
            }
        }
        public override void Unbind()
        {
            if (IsBound)
            {
                Gl.UseProgram(0);
                IsBound = false;
            }
        }

        public override void Upload(string name, float value)
        {
            int location = Gl.GetUniformLocation(_ProgramID, name);
            Gl.Uniform1f(location, value);
        }

        public override void Upload(string name, ref Vector3f values)
        {
            int location = Gl.GetUniformLocation(_ProgramID, name);
            Gl.Uniform3f(location, values.X, values.Y, values.Z);
        }

        public override void Upload(string name, ref Vector4f values)
        {
            int location = Gl.GetUniformLocation(_ProgramID, name);
            Gl.Uniform4f(location, values.X, values.Y, values.Z, values.W);
        }

        public override void Upload(string name, int value)
        {
            int location = Gl.GetUniformLocation(_ProgramID, name);
            Gl.Uniform1i(location, value);
        }

        public override void Upload(string name, int[] values)
        {
            int location = Gl.GetUniformLocation(_ProgramID, name);
            Gl.Uniform1iv(location, values.Length, values);
        }

        public override void Upload(string name, ref Matrix3f matrix)
        {
            int location = Gl.GetUniformLocation(_ProgramID, name);
            Gl.UniformMatrix3fv(location, 1, false, ref matrix.R1.X);
        }

        public override void Upload(string name, ref Matrix4f matrix)
        {
            int location = Gl.GetUniformLocation(_ProgramID, name);
            Gl.UniformMatrix4fv(location, 1, false, ref matrix.R1.X);
        }

        protected override void Destroy()
        {
            Gl.DeleteProgram(_ProgramID);
        }
    }
}
