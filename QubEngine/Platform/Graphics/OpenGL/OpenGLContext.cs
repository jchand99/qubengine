using System;

using QubEngine.Core.Rendering;

using Magma.Logger;
using Magma.OpenGL;
using Magma.GLFW;

namespace QubEngine.Platform.Graphics.OpenGL
{
    public class OpenGLContext : GraphicsContext
    {
        private IntPtr _WindowHandle;
        public OpenGLContext(IntPtr windowHandle) : base()
        {
            if (windowHandle == IntPtr.Zero) Log.Fatal<OpenGLContext>("Window handle is NULL!");
            else _WindowHandle = windowHandle;

            Glfw.MakeContextCurrent(_WindowHandle);

            string version = Gl.GetString(StringName.Version);
            Log.Info<OpenGLContext>("OpenGL Info:");
            Log.Info<OpenGLContext>($"  Vendor: {Gl.GetString(StringName.Vendor)}");
            Log.Info<OpenGLContext>($"  Renderer: {Gl.GetString(StringName.Renderer)}");
            Log.Info<OpenGLContext>($"  Version: {version}");

            Gl.GetIntegerv(GetPName.MajorVersion, out var major);
            Gl.GetIntegerv(GetPName.MinorVersion, out var minor);

            Log.Assert<OpenGLContext>((major[0] >= 4 && minor[0] >= 5), "Requires OpenGL 4.5 or higher!");
        }

        public override void SwapBuffers()
        {
            Glfw.SwapBuffers(_WindowHandle);
        }
    }
}
