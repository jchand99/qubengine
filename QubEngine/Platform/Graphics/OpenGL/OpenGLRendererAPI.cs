using System;
using Magma.Maths;
using Magma.OpenGL;

using QubEngine.Core.Rendering;

namespace QubEngine.Platform.Graphics.OpenGL
{
    public class OpenGLRendererAPI : RendererAPI
    {
        public OpenGLRendererAPI()
        {
            Gl.Enable(EnableCap.Blend);
            Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            Gl.Enable(EnableCap.DepthTest);
            Gl.Enable(EnableCap.CullFace);
            Gl.CullFace(CullFaceMode.Back);
        }

        public override void SetViewport(int x, int y, int width, int height)
        {
            Gl.Viewport(x, y, width, height);
        }

        public override void SetClearColor(Vector4f color)
        {
            Gl.ClearColor(color.X, color.Y, color.Z, color.W);
        }

        public override void Clear()
        {
            Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        public override void DrawIndexed(VertexArray vertexArray, uint indexCount)
        {
            int count = indexCount > 0 ? (int)indexCount : (int)vertexArray.GetIndexBufferCount();
            Gl.DrawElements(BeginMode.Triangles, count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            // Gl.BindTexture(TextureTarget.Texture2D, 0);
        }
    }


}
