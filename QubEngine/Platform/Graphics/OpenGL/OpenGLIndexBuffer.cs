using System;
using Magma.OpenGL;
using QubEngine.Core.Rendering;

namespace QubEngine.Platform.Graphics.OpenGL
{
    public class OpenGLIndexBuffer : IndexBuffer
    {
        private uint _BufferID;
        public bool IsBound { get; private set; }

        public OpenGLIndexBuffer(Span<uint> data)
        {
            Gl.CreateBuffer(out _BufferID);

            Gl.BindBuffer(BufferTarget.ElementArrayBuffer, _BufferID);
            Count = data.Length;
            Gl.BufferData<uint>(BufferTarget.ElementArrayBuffer, sizeof(uint) * Count, data, BufferUsageHint.StaticDraw);
            IsBound = true;
        }

        public override void Bind()
        {
            if (!IsBound)
            {
                Gl.BindBuffer(BufferTarget.ElementArrayBuffer, _BufferID);
                IsBound = true;
            }
        }

        public override void Unbind()
        {
            if (IsBound)
            {
                Gl.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
                IsBound = false;
            }
        }

        public override void Destroy()
        {
            Gl.DeleteBuffer(_BufferID);
        }
    }
}
