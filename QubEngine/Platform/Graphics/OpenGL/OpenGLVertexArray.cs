using System.Collections.Generic;
using QubEngine.Core.Rendering;
using Magma.OpenGL;

namespace QubEngine.Platform.Graphics.OpenGL
{
    public class OpenGLVertexArray : VertexArray
    {
        public bool IsBound { get; private set; }
        private List<VertexBuffer> _VertexBuffers;
        private IndexBuffer _IndexBuffer;
        private uint _ArrayID;

        public OpenGLVertexArray()
        {
            _VertexBuffers = new List<VertexBuffer>();
            Gl.CreateVertexArray(out _ArrayID);
            IsBound = false;
        }

        public override void AddVertexBuffer(VertexBuffer buffer)
        {
            Gl.BindVertexArray(_ArrayID);
            buffer.Bind();
            BufferLayout bufferLayout = buffer.Layout;
            for (int i = 0; i < bufferLayout.Length; i++)
            {
                BufferElement element = bufferLayout[i];
                Gl.EnableVertexAttribArray((uint)i);
                Gl.VertexAttribPointer((uint)i, (int)element.ComponentCount, element.VertexAttribType, element.Normalized, (int)bufferLayout.Stride, (int)element.Offset);
            }
            _VertexBuffers.Add(buffer);
            Gl.BindVertexArray(0);
        }

        public override void Bind()
        {
            if (!IsBound)
            {
                Gl.BindVertexArray(_ArrayID);
                IsBound = true;
            }
        }
        public override void Unbind()
        {
            if (IsBound)
            {
                Gl.BindVertexArray(0);
                IsBound = false;
            }
        }

        public override void SetIndexBuffer(IndexBuffer buffer)
        {
            Gl.BindVertexArray(_ArrayID);
            buffer.Bind();
            _IndexBuffer = buffer;
            Gl.BindVertexArray(0);
        }

        public override void Destroy()
        {
            for (int i = 0; i < _VertexBuffers.Count; i++)
            {
                _VertexBuffers[i].Dispose();
            }
            _IndexBuffer.Dispose();
            Gl.DeleteVertexArray(_ArrayID);
        }

        public override uint GetIndexBufferCount() => (uint)_IndexBuffer.Count;
    }
}
