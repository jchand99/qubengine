using System;
using Magma.GLFW;
using Magma.Logger;
using QubEngine.Core;
using QubEngine.Core.Events;
using QubEngine.Core.Rendering;

namespace QubEngine.Platform.OS.Windows
{
    public class WindowsWindow : Window
    {
        private GraphicsContext _GraphicsContext;
        private Action<Event> _EventCallback;

        public WindowsWindow(WindowProps props)
        {
            Width = props.Width;
            Height = props.Height;
            Title = props.Title;

            if (WindowCount == 0)
            {
                bool status = Glfw.Init();
                Log.Assert<WindowsWindow>(status, "Could not initialize GLFW!");
                Glfw.SetErrorCallback(ErrorCallback);
            }

            Handle = Glfw.CreateWindow(Width, Height, Title, IntPtr.Zero, IntPtr.Zero);
            IsValid = Handle == IntPtr.Zero ? false : true;
            ++WindowCount;

            Input.Init(new WindowsInput());
            _GraphicsContext = GraphicsContext.Create(Handle);

            Glfw.SetKeyCallback(Handle, (window, key, scancode, buttonState, mods) =>
            {
                switch (buttonState)
                {
                    case ButtonState.Press:
                        {
                            KeyPressedEvent keyPressedEvent = new KeyPressedEvent((Key)key, 0);
                            _EventCallback(keyPressedEvent);
                            break;
                        }
                    case ButtonState.Release:
                        {
                            KeyReleasedEvent keyReleasedEvent = new KeyReleasedEvent((Key)key);
                            _EventCallback(keyReleasedEvent);
                            break;
                        }
                    case ButtonState.Repeat:
                        {
                            KeyPressedEvent keyPressedEvent = new KeyPressedEvent((Key)key, 1);
                            _EventCallback(keyPressedEvent);
                            break;
                        }
                }
            });

            Glfw.SetMouseButtonCallback(Handle, (window, button, buttonState, mods) =>
            {
                switch (buttonState)
                {
                    case ButtonState.Press:
                        {
                            MouseButtonPressedEvent mouseButtonPressedEvent = new MouseButtonPressedEvent(button);
                            _EventCallback(mouseButtonPressedEvent);
                            break;
                        }
                    case ButtonState.Release:
                        {
                            MouseButtonReleasedEvent mouseButtonReleased = new MouseButtonReleasedEvent(button);
                            _EventCallback(mouseButtonReleased);
                            break;
                        }
                }
            });

            Glfw.SetCursorPosCallback(Handle, (window, xPos, yPos) =>
            {
                MouseMovedEvent mouseMovedEvent = new MouseMovedEvent((float)xPos, (float)yPos);
                _EventCallback(mouseMovedEvent);
            });

            Glfw.SetScrollCallback(Handle, (window, xOffset, yOffset) =>
            {
                MouseScrolledEvent mouseScrolledEvent = new MouseScrolledEvent((float)xOffset, (float)yOffset);
                _EventCallback(mouseScrolledEvent);
            });

            Glfw.SetWindowCloseCallback(Handle, (window) =>
            {
                WindowCloseEvent windowClose = new WindowCloseEvent();
                _EventCallback(windowClose);
            });
        }

        public override void OnUpdate()
        {
            Glfw.PollEvents();
            _GraphicsContext.SwapBuffers();
        }

        public override void SetVSync(bool enabled)
        {
            if (enabled) Glfw.SwapInterval(1);
            else Glfw.SwapInterval(0);
            IsVsync = enabled;
        }

        public override void Close()
        {
            Glfw.DestroyWindow(Handle);
            --WindowCount;
            if (WindowCount == 0) Glfw.Terminate();
        }

        public override void SetEventCallback(Action<Event> callback) => _EventCallback = callback;
        private static void ErrorCallback(int error, string description)
        {
            Log.Error<WindowsWindow>($"GLFW Error {error}: {description}");
        }
    }
}
