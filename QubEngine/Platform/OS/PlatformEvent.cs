using System;
using Magma.GLFW;

namespace QubEngine.Platform.OS
{
    public class KeyPressEventArgs : EventArgs
    {
        public string Info { get; set; }
        public KeyCode Key { get; set; }
        public ButtonState ButtonState { get; } = ButtonState.Press;

        public KeyPressEventArgs(string info, KeyCode key)
        {
            Info = info;
            Key = key;
        }
    }

    public class KeyReleaseEventArgs : EventArgs
    {
        public string Info { get; set; }
        public KeyCode Key { get; set; }
        public ButtonState ButtonState { get; } = ButtonState.Release;

        public KeyReleaseEventArgs(string info, KeyCode key)
        {
            Info = info;
            Key = key;
        }
    }

    public class KeyRepeatEventArgs : EventArgs
    {
        public string Info { get; set; }
        public KeyCode Key { get; set; }
        public ButtonState ButtonState { get; } = ButtonState.Repeat;

        public KeyRepeatEventArgs(string info, KeyCode key)
        {
            Info = info;
            Key = key;
        }
    }

    public class CloseEventArgs : EventArgs
    {
        public string Info { get; set; }

        public CloseEventArgs(string info) { Info = info; }
    }

    public class MousePressEventArgs : EventArgs
    {
        public string Info { get; set; }
        public MouseButton Button { get; set; }

        public MousePressEventArgs(string info, MouseButton button)
        {
            Info = info;
            Button = button;
        }
    }

    public class MouseReleaseEventArgs : EventArgs
    {
        public string Info { get; set; }
        public MouseButton Button { get; set; }

        public MouseReleaseEventArgs(string info, MouseButton button)
        {
            Info = info;
            Button = button;
        }
    }

    public class MouseScrollEventArgs : EventArgs
    {
        public string Info { get; set; }
        public float XOffset { get; set; }
        public float YOffset { get; set; }

        public MouseScrollEventArgs(string info, float xOffset, float yOffset)
        {
            Info = info;
            XOffset = xOffset;
            YOffset = yOffset;
        }
    }

    public class CursorPosEventArgs : EventArgs
    {
        public string Info { get; set; }
        public float X { get; set; }
        public float Y { get; set; }

        public CursorPosEventArgs(string info, float x, float y)
        {
            Info = info;
            X = x;
            Y = y;
        }
    }
}
