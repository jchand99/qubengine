using Magma.GLFW;
using Magma.Maths;

using QubEngine.Core;

namespace QubEngine.Platform.OS
{
    public class LinuxInput : Input
    {
        public override bool IsKeyPressedImpl(Key key)
        {
            var window = Application.Instance.Window;
            var state = Glfw.GetKey(window.Handle, (KeyCode)key);
            return state == ButtonState.Press || state == ButtonState.Repeat;
        }

        public override bool IsMouseButtonPressedImpl(MButton button)
        {
            var window = Application.Instance.Window;
            return Glfw.GetMouseButton(window.Handle, (MouseButton)button) == ButtonState.Press;
        }

        public override Vector2f GetMousePositionImpl()
        {
            var window = Application.Instance.Window;
            Glfw.GetCursorPos(window.Handle, out var xpos, out var ypos);
            return new Vector2f((float)xpos, (float)ypos);
        }

        public override float GetMouseXImpl() => GetMousePositionImpl().X;
        public override float GetMouseYImpl() => GetMousePositionImpl().Y;
    }
}
