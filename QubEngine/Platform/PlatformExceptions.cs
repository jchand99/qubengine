using System;
using System.Collections;
using System.Runtime.Serialization;

namespace QubEngine.Platform
{
    internal class QubEnginePlatformException : QubEngineException
    {
        internal QubEnginePlatformException() { }
        internal QubEnginePlatformException(string message) : base(message) { }
        internal QubEnginePlatformException(object thrower, string message) : base(thrower, message) { }
        internal QubEnginePlatformException(string message, Exception innerException) : base(message, innerException) { }
        internal QubEnginePlatformException(object thrower, string message, Exception innerException) : base(thrower, message, innerException) { }
        protected QubEnginePlatformException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public override IDictionary Data => base.Data;
        public override string HelpLink { get => base.HelpLink; set => base.HelpLink = value; }
        public override string Message => base.Message;
        public override string Source { get => base.Source; set => base.Source = value; }
        public override string StackTrace => base.StackTrace;

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Exception GetBaseException()
        {
            return base.GetBaseException();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }

    internal class QubEngineShaderException : QubEnginePlatformException
    {
        protected QubEngineShaderException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        internal QubEngineShaderException() { }
        internal QubEngineShaderException(string message) : base(message) { }
        internal QubEngineShaderException(object thrower, string message) : base(thrower, message) { }
        internal QubEngineShaderException(string message, Exception innerException) : base(message, innerException) { }
        internal QubEngineShaderException(object thrower, string message, Exception innerException) : base(thrower, message, innerException) { }

        public override IDictionary Data => base.Data;
        public override string HelpLink { get => base.HelpLink; set => base.HelpLink = value; }
        public override string Message => base.Message;
        public override string Source { get => base.Source; set => base.Source = value; }
        public override string StackTrace => base.StackTrace;

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Exception GetBaseException()
        {
            return base.GetBaseException();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
