using QubEngine.Core;

namespace Qube
{
    public class Sandbox : Application
    {
        public Sandbox(string title) : base(title)
        {
            PushLayer(new ExampleLayer("ExampleLayer"));
        }
    }
}
