using System;
using System.Diagnostics;
using Magma.Logger;
using Magma.Maths;
using Qube.Enviro;
using Qube.Enviro.Meshes;
using Qube.Util.Noise;
using QubEngine.Core;
using QubEngine.Core.Events;
using QubEngine.Core.Layers;
using QubEngine.Core.Rendering;

namespace Qube
{
    public class ExampleLayer : Layer
    {
        private PerspectiveCameraController _PerController;
        private OrthographicCameraController _OrthoController;
        private Window _Window;
        static Matrix4f Ident = Matrix4f.Identity;
        private World _World;
        // private Texture2D _NoiseMap;
        public ExampleLayer(string name) : base(name)
        {
            Name = name;
        }

        public override void OnAttach()
        {
            Log.Debug<ExampleLayer>("ExampleLayer Attached");

            float aspectRatio = 1.6f / 0.9f;
            _PerController = new PerspectiveCameraController(45.0f.ToRadians(), aspectRatio);
            _OrthoController = new OrthographicCameraController(aspectRatio);
            _PerController.SetPosition(new Vector3f(0.0f, 0.0f, 0.0f));
            _Window = Application.Instance.Window;

            _World = new World(16, 16);

            // int w = 256;
            // int h = 256;
            // Noise noise = new Noise(w, h);
            // byte[] arr = new byte[w * h * 4];
            // int index = 0;
            // for (int i = 0; i < w * h; i++)
            // {
            //     // float y = (float)_Noise.getHeight(x, z);
            //     // uint dy = (uint)(_Noise.getHeight(x, z));
            //     byte br = (byte)(noise[i] * 120 + 128);
            //     // uint y = dy > 0 ? (uint)dy : 0;
            //     // int y = (x + z);
            //     byte color = (byte)((255 << 24) | (br << 16) | (br << 8) | br);

            //     // Console.WriteLine(color.ToString("X"));
            //     arr[index] = color;
            //     arr[index + 1] = color;
            //     arr[index + 2] = color;
            //     arr[index + 3] = color;
            //     index += 4;
            // }

            // _NoiseMap = Texture2D.Create(w, h);
            // // _NoiseMap.SetData<uint>(0xffffffff, sizeof(uint));
            // _NoiseMap.SetData<byte>(arr);
        }

        public override void OnDetach()
        {
            Log.Debug<ExampleLayer>("ExampleLayer Detached");
            // _NoiseMap.Dispose();
        }

        public override void OnUpdate(TimeStep timeStep)
        {
            _PerController.OnUpdate(timeStep);
            // _OrthoController.OnUpdate(timeStep);
            PlatformCommand.Clear();
            PlatformCommand.ClearColor(new Vector4f(0.2f, 0.2f, 0.2f, 1.0f));

            // Renderer2D.BeginScene(_OrthoController);
            // Renderer2D.DrawQuad(new Vector2f(0.0f), new Vector2f(1.0f), _NoiseMap);
            // Renderer2D.EndScene();
            Renderer.BeginScene(_PerController);
            Renderer.Submit(_World.Mesh, ref Ident);
            Renderer.EndScene();
        }

        public override void OnEvent(Event e)
        {
            EventDispatcher eventDispatcher = new EventDispatcher(e);
            eventDispatcher.Dispatch<KeyEvent>(OnKeyPressed);
            _PerController.OnEvent(e);
            // _OrthoController.OnEvent(e);
        }

        private bool OnKeyPressed(KeyEvent arg)
        {
            switch (arg.Key)
            {
                case Key.H:
                    _Window.HideCursor();
                    break;
                case Key.J:
                    _Window.DisableCursor();
                    break;
                case Key.K:
                    _Window.EnableCursor();
                    break;
            }
            return false;
        }
    }
}
