using System;
using System.Collections.Generic;
using QubEngine.Core;
using QubEngine.Core.Rendering;

namespace Qube
{
    [Flags]
    public enum Tiles : int
    {
        None = 0,
        Water = 1,
        Grass = 2,
        GrassBottom = 4,
        GrassBottomLeft = 8,
        GrassBottomRight = 16,
        GrassTop = 32,
        GrassTopLeft = 64,
        GrassTopRight = 128,
        GrassLeft = 256,
        GrassRight = 512,
    }

    public class TileManager
    {
        private Dictionary<Tiles, SubTexture2D> _Tiles;

        public SubTexture2D this[Tiles key]
        {
            get { return _Tiles[key]; }
        }

        public TileManager(Texture2D texture)
        {
            _Tiles = new Dictionary<Tiles, SubTexture2D>();
            _Tiles.Add(Tiles.None, SubTexture2D.CreateFromCoordinates(texture, new(16, 2), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.Water, SubTexture2D.CreateFromCoordinates(texture, new(16, 0), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.Grass, SubTexture2D.CreateFromCoordinates(texture, new(6, 10), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.GrassBottom, SubTexture2D.CreateFromCoordinates(texture, new(6, 9), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.GrassTop, SubTexture2D.CreateFromCoordinates(texture, new(6, 11), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.GrassLeft, SubTexture2D.CreateFromCoordinates(texture, new(5, 10), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.GrassRight, SubTexture2D.CreateFromCoordinates(texture, new(7, 10), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.GrassBottomLeft, SubTexture2D.CreateFromCoordinates(texture, new(5, 9), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.GrassBottomRight, SubTexture2D.CreateFromCoordinates(texture, new(7, 9), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.GrassTopLeft, SubTexture2D.CreateFromCoordinates(texture, new(5, 11), new(128, 128), new(1, 1)));
            _Tiles.Add(Tiles.GrassTopRight, SubTexture2D.CreateFromCoordinates(texture, new(7, 11), new(128, 128), new(1, 1)));
        }
    }
}
