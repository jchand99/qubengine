﻿namespace Qube
{
    class Program
    {
        static void Main(string[] args)
        {
            Sandbox sandbox = new Sandbox("Game");
            sandbox.Run();
        }
    }
}
