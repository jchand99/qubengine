using System;
using System.Threading;
using System.Diagnostics;
using Magma.Logger;
using Magma.Maths;
using Qube.Enviro.Meshes;
using QubEngine.Core.Rendering;
using System.Threading.Tasks;

namespace Qube.Enviro
{
    public class World
    {
        public WorldMesh Mesh { get; private set; }
        private int _Width;
        private int _Length;

        public World(int width, int length)
        {
            _Width = width;
            _Length = length;
            Mesh = new WorldMesh(new Material(Shader.Create("assets/shaders/TestMeshShader.glsl")));
            Mesh.GenerateHeightMap(width, length);
            Generate();

            // Task task = new Task(Generate);
            // task.Start();
            // task.Wait();
            // Thread t = new Thread(Generate);
            // t.Start();
            // t.Join();

            Mesh.Buffer();
        }

        private void Generate()
        {
            Stopwatch sw = new Stopwatch();
            sw.Restart();
            for (int x = 0; x < _Width; x++)
                for (int z = 0; z < _Length; z++)
                {
                    Mesh.AddChunkAtPosition(new Vector2i(x, z));
                }
            Mesh.AddFacesToChunks();
            sw.Stop();
            Log.Debug<ExampleLayer>($"Took: {sw.Elapsed.Milliseconds}ms");
        }
    }
}
