using Magma.Maths;

namespace Qube.Enviro
{
    public class Block
    {
        public Vector3i Position { get; private set; }
        public BlockID ID { get; private set; }
        public bool IsTransparent { get; private set; }

        public Block(Vector3i position, BlockID id, bool transparent = false)
        {
            Position = position;
            ID = id;
            IsTransparent = transparent;
        }
    }
    public enum BlockID
    {
        Unkown = 0,
        Air = 1,
        Grass = 2,
        Stone = 3,
        Sand = 4,
    }

    public struct BlockData
    {
        public BlockID ID { get; }
        public Vector3f Position { get; }
        public bool IsTransparent { get; }
    }
}
