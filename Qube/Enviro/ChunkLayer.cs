using System.Collections.Generic;
using Magma.Maths;
using static Qube.Enviro.WorldConstants;

namespace Qube.Enviro
{
    public class ChunkLayer
    {
        private Dictionary<int, Block> _Blocks;
        public int Y { get; private set; }
        public int NumTransparentBlocks { get; private set; }

        public ChunkLayer(int y)
        {
            Y = y;
            _Blocks = new Dictionary<int, Block>(CHUNK_LAYER_SIZE);
        }

        public Block GetBlock(int id) => _Blocks.GetValueOrDefault(id);

        public void AddBlock(int blockPositionID, Block block)
        {
            if (block.IsTransparent) NumTransparentBlocks++;

            _Blocks.Add(blockPositionID, block);
        }
    }
}
