using System;
using System.Collections.Generic;
using Magma.Maths;
using Qube.Util;
using Qube.Util.Noise;
using QubEngine.Core.Rendering;
using static Qube.Enviro.WorldConstants;


namespace Qube.Enviro.Meshes
{
    public class WorldMesh : Mesh
    {
        private List<float> _Vertices;
        private List<uint> _Indices;
        private Dictionary<int, Chunk> _Chunks;
        public object MeshLock = new object();
        public Dictionary<int, int> HeightMap;

        public WorldMesh(Material material) : base(material)
        {
            _Vertices = new List<float>();
            _Indices = new List<uint>();
            _Chunks = new Dictionary<int, Chunk>();
        }

        public void GenerateHeightMap(int width, int length)
        {
            int w = width * CHUNK_SIZE;
            int l = length * CHUNK_SIZE;

            OpenSimplexNoise osn = new OpenSimplexNoise();
            // Console.WriteLine(osn.Evaluate(745.0f, 232.0f));

            // Noise noise = new Noise(w, l);
            HeightMap = new Dictionary<int, int>();
            for (int j = 0; j < l; j++)
                for (int i = 0; i < w; i++)
                {
                    int index = j + i * w;

                    // HeightMap.Add(Hasher.Hash(i, j), (int)(noise[index] * 64 + 64));
                    HeightMap.Add(Hasher.Hash(i, j), (int)(((osn.Evaluate(i * 0.01f, j * 0.01f) + 1) * 127.5)));
                }
        }

        public void Buffer()
        {
            uint offset = 0;
            uint[] indices = new uint[IndicesCount];
            for (int i = 0; i < IndicesCount; i += 6)
            {
                indices[i + 0] = offset + 0;
                indices[i + 1] = offset + 1;
                indices[i + 2] = offset + 2;

                indices[i + 3] = offset + 2;
                indices[i + 4] = offset + 3;
                indices[i + 5] = offset + 0;
                offset += 4;
            }

            AddData(_Vertices.ToArray(), indices, new BufferLayout() {
                new BufferElement(ShaderDataType.Float4, "a_Pos"),
                new BufferElement(ShaderDataType.Float4, "a_Color"),
            });
        }

#nullable enable
        public Chunk? GetChunk(int id)
        {
            _Chunks.TryGetValue(id, out Chunk? c);
            return c;
        }
#nullable disable

        public void AddVertices(float[] vertices)
        {
            _Vertices.AddRange(vertices);
            IndicesCount += 6;
        }

        // public void GenerateChunkAt(Vector2i chunkPosition)
        // {
        //     Chunk c = new Chunk(chunkPosition, this);
        //     c.GenerateLayers(this);

        //     _Chunks.Add(c.ID, c);
        // }

        // public void AddFacesToChunk()
        // {
        //     foreach (Chunk c in _Chunks.Values)
        //     {
        //         c.AddFaces(this);
        //     }
        // }

        public void AddFacesToChunks()
        {
            foreach (Chunk chunk in _Chunks.Values)
            {
                chunk.AddFaces(this);
            }
        }

        public void AddChunkAtPosition(Vector2i chunkPosition)
        {
            _Chunks.Add(Hasher.Hash(chunkPosition.X, chunkPosition.Y), new Chunk(chunkPosition, this));
        }
    }
}
