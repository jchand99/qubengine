namespace Qube.Enviro
{
    public static class WorldConstants
    {
        public const float BLOCK_SIZE = 0.5f;
        public const int CHUNK_SIZE = 16;
        public const int CHUNK_HEIGHT = 256;
        public const int CHUNK_SIZE_HALF = CHUNK_SIZE / 2;
        public const int CHUNK_LAYER_SIZE = CHUNK_SIZE * CHUNK_SIZE;
        public const int CHUNK_LAYER_COUNT = CHUNK_HEIGHT;
    }
}
