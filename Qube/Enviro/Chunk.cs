using System;
using System.Collections.Generic;

using Magma.Maths;
using Qube.Util;
using Qube.Util.Noise;
using Qube.Enviro.Meshes;

using static Qube.Enviro.WorldConstants;
using Magma.Logger;

namespace Qube.Enviro
{
    public class Chunk
    {
        private ChunkLayer[] _Layers;
        private Vector2i _Position;
        public int ID { get; private set; }

        public Chunk(Vector2i position, WorldMesh mesh)
        {
            ID = Hasher.Hash(position.X, position.Y);
            _Position = position;
            _Layers = new ChunkLayer[CHUNK_LAYER_COUNT];

            GenerateLayers(mesh);
        }

        public void AddFaces(WorldMesh mesh)
        {
            // int nLayerIndex = 1;
            // int drawingLayerIndex = 0;
            int cx = _Position.X;
            int cz = _Position.Y;
            int xChunkOrigin = CHUNK_SIZE * cx;
            int zChunkOrigin = CHUNK_SIZE * cz;
            int xBounds = xChunkOrigin + CHUNK_SIZE;
            int zBounds = zChunkOrigin + CHUNK_SIZE;

            for (int i = 0; i < _Layers.Length - 1; i++)
            {
                // FIgure out vulkan first then write game.
                ChunkLayer currentLayer = _Layers[i];
                ChunkLayer aboveLayer = _Layers[i + 1];
                ChunkLayer leftLayer = mesh.GetChunk(Hasher.Hash(cx - 1, cz))?._Layers[i];
                ChunkLayer rightLayer = mesh.GetChunk(Hasher.Hash(cx + 1, cz))?._Layers[i];
                ChunkLayer frontLayer = mesh.GetChunk(Hasher.Hash(cx - 1, cz))?._Layers[i];
                ChunkLayer backLayer = mesh.GetChunk(Hasher.Hash(cx + 1, cz))?._Layers[i];

                int transparentBlocks = currentLayer.NumTransparentBlocks + aboveLayer.NumTransparentBlocks +
                leftLayer?.NumTransparentBlocks ?? 0 + rightLayer?.NumTransparentBlocks ?? 0 + frontLayer?.NumTransparentBlocks ?? 0
                + backLayer?.NumTransparentBlocks ?? 0;

                if (transparentBlocks > 0)
                {
                    for (int z = 0; z < CHUNK_SIZE; z++)
                        for (int x = 0; x < CHUNK_SIZE; x++)
                        {
                            int xPos = x + xChunkOrigin;
                            int zPos = z + zChunkOrigin;
                            int yPos = currentLayer.Y;

                            int topHash = Hasher.Hash(xPos, yPos + 1, zPos);
                            int bottomHash = Hasher.Hash(xPos, yPos - 1, zPos);
                            int leftHash = Hasher.Hash(xPos - 1, yPos, zPos);
                            int rightHash = Hasher.Hash(xPos + 1, yPos, zPos);
                            int frontHash = Hasher.Hash(xPos, yPos, zPos - 1);
                            int backHash = Hasher.Hash(xPos, yPos, zPos + 1);

                            Block currentBlock = currentLayer.GetBlock(Hasher.Hash(xPos, yPos, zPos));

                            if (!currentBlock.IsTransparent)
                            {
                                Block topBlock = aboveLayer.GetBlock(topHash);
                                Block bottomBlock = _Layers[i - 1].GetBlock(bottomHash);

                                Block leftBlock = currentLayer.GetBlock(leftHash);
                                if (leftBlock == null)
                                {
                                    Chunk c = mesh.GetChunk(Hasher.Hash(cx - 1, cz));
                                    leftBlock = c?._Layers[i].GetBlock(leftHash);
                                }

                                Block rightBlock = currentLayer.GetBlock(rightHash);
                                if (rightBlock == null)
                                {
                                    Chunk c = mesh.GetChunk(Hasher.Hash(cx + 1, cz));
                                    rightBlock = c?._Layers[i].GetBlock(rightHash);
                                }

                                Block frontBlock = currentLayer.GetBlock(frontHash);
                                if (frontBlock == null)
                                {
                                    Chunk c = mesh.GetChunk(Hasher.Hash(cx, cz - 1));
                                    frontBlock = c?._Layers[i].GetBlock(frontHash);
                                }

                                Block backBlock = currentLayer.GetBlock(backHash);
                                if (backBlock == null)
                                {
                                    Chunk c = mesh.GetChunk(Hasher.Hash(cx, cz + 1));
                                    backBlock = c?._Layers[i].GetBlock(backHash);
                                }

                                if (leftBlock == null || leftBlock.IsTransparent) mesh.AddVertices(LeftFace(xPos, yPos, zPos));
                                if (rightBlock == null || rightBlock.IsTransparent) mesh.AddVertices(RightFace(xPos, yPos, zPos));
                                if (frontBlock == null || frontBlock.IsTransparent) mesh.AddVertices(FrontFace(xPos, yPos, zPos));
                                if (backBlock == null || backBlock.IsTransparent) mesh.AddVertices(BackFace(xPos, yPos, zPos));
                                if (topBlock == null || topBlock.IsTransparent) mesh.AddVertices(TopFace(xPos, yPos, zPos));
                                if (bottomBlock == null || bottomBlock.IsTransparent) mesh.AddVertices(BottomFace(xPos, yPos, zPos));
                            }
                        }
                }
            }

            // for (int i = 1; i < _Layers.Length - 1; i++)
            // {
            //     ChunkLayer nLayer = _Layers[i];
            //     ChunkLayer bLayer = _Layers[i - 1];
            //     if (nLayer.NumTransparentBlocks > 0 || bLayer.NumTransparentBlocks > 0)
            //     {

            //         // ChunkLayer la = _Layers[i + 1];
            //         ChunkLayer lb = _Layers[i - 2];

            //         // FIXME: faces not being added correctly.
            //         // Checking only current layer. may want to check previous layer and future layer
            //         // to fix the issue. not adding faces to previous layer because it has a 100% opaque
            //         // block count.
            //         for (int z = 0; z < CHUNK_SIZE; z++)
            //             for (int x = 0; x < CHUNK_SIZE; x++)
            //             {
            //                 int xPos = x + xChunkOrigin;
            //                 int zPos = z + zChunkOrigin;
            //                 int y = bLayer.Y;
            //                 int hc = Hasher.Hash(xPos, y, zPos);
            //                 Block self = bLayer[hc];
            //                 if (!self.IsTransparent)
            //                 {
            //                     int tophc = Hasher.Hash(xPos, y + 1, zPos);
            //                     int bottomhc = Hasher.Hash(xPos, y - 1, zPos);
            //                     int lefthc = Hasher.Hash(xPos - 1, y, zPos);
            //                     int righthc = Hasher.Hash(xPos + 1, y, zPos);
            //                     int fronthc = Hasher.Hash(xPos, y, zPos - 1);
            //                     int backhc = Hasher.Hash(xPos, y, zPos + 1);

            //                     Block top = nLayer[tophc];
            //                     Block bottom = lb[bottomhc];

            //                     Block left = nLayer[lefthc];
            //                     if (left == null)
            //                     {
            //                         Chunk c = mesh.GetChunk(Hasher.Hash(cx - 1, cz));
            //                         if (c != null)
            //                         {
            //                             ChunkLayer l = c._Layers[i];
            //                             left = l[lefthc];
            //                         }
            //                     }

            //                     Block right = nLayer[righthc];
            //                     if (right == null)
            //                     {
            //                         Chunk c = mesh.GetChunk(Hasher.Hash(cx + 1, cz));
            //                         if (c != null)
            //                         {
            //                             ChunkLayer l = c._Layers[i];
            //                             right = l[righthc];
            //                         }
            //                     }

            //                     Block front = nLayer[fronthc];
            //                     if (front == null)
            //                     {
            //                         Chunk c = mesh.GetChunk(Hasher.Hash(cx, cz - 1));
            //                         if (c != null)
            //                         {
            //                             ChunkLayer l = c._Layers[i];
            //                             front = l[fronthc];
            //                         }
            //                     }

            //                     Block back = nLayer[backhc];
            //                     if (back == null)
            //                     {
            //                         Chunk c = mesh.GetChunk(Hasher.Hash(cx, cz + 1));
            //                         if (c != null)
            //                         {
            //                             ChunkLayer l = c._Layers[i];
            //                             back = l[backhc];
            //                         }
            //                     }

            //                     // CHECK IF ALL BLOCK FACES ARE NULL
            //                     if (top == null && bottom == null && left == null && right == null && front == null && back == null)
            //                     {
            //                         Log.Trace<Chunk>("All are null!");
            //                         Log.Trace<Chunk>($"xPos: {xPos}, zPos: {zPos}");
            //                         // Log.Trace<Chunk>($"xzPos: {xPos}, zPos: {zPos}");
            //                     }

            //                     if (top != null && top.IsTransparent)
            //                     {
            //                         mesh.AddVertices(TopFace(xPos, y, zPos));
            //                     }
            //                     if (bottom != null && bottom.IsTransparent)
            //                     {
            //                         mesh.AddVertices(BottomFace(xPos, y, zPos));
            //                     }
            //                     if (left != null && left.IsTransparent)
            //                     {
            //                         mesh.AddVertices(LeftFace(xPos, y, zPos));
            //                     }
            //                     if (right != null && right.IsTransparent)
            //                     {
            //                         mesh.AddVertices(RightFace(xPos, y, zPos));
            //                     }
            //                     if (front != null && front.IsTransparent)
            //                     {
            //                         mesh.AddVertices(FrontFace(xPos, y, zPos));
            //                     }
            //                     if (back != null && back.IsTransparent)
            //                     {
            //                         mesh.AddVertices(BackFace(xPos, y, zPos));
            //                     }
            //                 }
            //                 // if (!self.IsTransparent)
            //                 // {
            //                 //     int tophc = Hasher.Hash(xPos, y + 1, zPos);
            //                 //     int bottomhc = Hasher.Hash(xPos, y - 1, zPos);
            //                 //     int lefthc = Hasher.Hash(xPos - 1, y, zPos);
            //                 //     int righthc = Hasher.Hash(xPos + 1, y, zPos);
            //                 //     int fronthc = Hasher.Hash(xPos, y, zPos - 1);
            //                 //     int backhc = Hasher.Hash(xPos, y, zPos + 1);

            //                 //     Block top = la[tophc];
            //                 //     Block bottom = lb[bottomhc];

            //                 //     Block left = nLayer[lefthc];
            //                 //     if (left == null)
            //                 //     {
            //                 //         Chunk c = mesh.GetChunk(Hasher.Hash(cx - 1, cz));
            //                 //         if (c != null)
            //                 //         {
            //                 //             ChunkLayer l = c._Layers[i];
            //                 //             left = l[lefthc];
            //                 //         }
            //                 //     }

            //                 //     Block right = nLayer[righthc];
            //                 //     if (right == null)
            //                 //     {
            //                 //         Chunk c = mesh.GetChunk(Hasher.Hash(cx + 1, cz));
            //                 //         if (c != null)
            //                 //         {
            //                 //             ChunkLayer l = c._Layers[i];
            //                 //             right = l[righthc];
            //                 //         }
            //                 //     }

            //                 //     Block front = nLayer[fronthc];
            //                 //     if (front == null)
            //                 //     {
            //                 //         Chunk c = mesh.GetChunk(Hasher.Hash(cx, cz - 1));
            //                 //         if (c != null)
            //                 //         {
            //                 //             ChunkLayer l = c._Layers[i];
            //                 //             front = l[fronthc];
            //                 //         }
            //                 //     }

            //                 //     Block back = nLayer[backhc];
            //                 //     if (back == null)
            //                 //     {
            //                 //         Chunk c = mesh.GetChunk(Hasher.Hash(cx, cz + 1));
            //                 //         if (c != null)
            //                 //         {
            //                 //             ChunkLayer l = c._Layers[i];
            //                 //             back = l[backhc];
            //                 //         }
            //                 //     }

            //                 //     // CHECK IF ALL BLOCK FACES ARE NULL
            //                 //     if (top == null && bottom == null && left == null && right == null && front == null && back == null)
            //                 //     {
            //                 //         Log.Trace<Chunk>("All are null!");
            //                 //         Log.Trace<Chunk>($"xPos: {xPos}, zPos: {zPos}");
            //                 //         // Log.Trace<Chunk>($"xzPos: {xPos}, zPos: {zPos}");
            //                 //     }

            //                 //     if (top != null && top.IsTransparent)
            //                 //     {
            //                 //         mesh.AddVertices(TopFace(xPos, y, zPos));
            //                 //     }
            //                 //     if (bottom != null && bottom.IsTransparent)
            //                 //     {
            //                 //         mesh.AddVertices(BottomFace(xPos, y, zPos));
            //                 //     }
            //                 //     if (left != null && left.IsTransparent)
            //                 //     {
            //                 //         mesh.AddVertices(LeftFace(xPos, y, zPos));
            //                 //     }
            //                 //     if (right != null && right.IsTransparent)
            //                 //     {
            //                 //         mesh.AddVertices(RightFace(xPos, y, zPos));
            //                 //     }
            //                 //     if (front != null && front.IsTransparent)
            //                 //     {
            //                 //         mesh.AddVertices(FrontFace(xPos, y, zPos));
            //                 //     }
            //                 //     if (back != null && back.IsTransparent)
            //                 //     {
            //                 //         mesh.AddVertices(BackFace(xPos, y, zPos));
            //                 //     }
            //                 // }
            //             }
            //     }
            // }
        }

        public void GenerateLayers(WorldMesh mesh)
        {
            int cx = _Position.X;
            int cz = _Position.Y;
            int xChunkOrigin = CHUNK_SIZE * cx;
            int zChunkOrigin = CHUNK_SIZE * cz;
            for (int y = 0; y < CHUNK_HEIGHT; y++)
            {
                ChunkLayer layer = new ChunkLayer(y);
                for (int z = 0; z < CHUNK_SIZE; z++)
                    for (int x = 0; x < CHUNK_SIZE; x++)
                    {
                        int xPos = x + xChunkOrigin;
                        int zPos = z + zChunkOrigin;
                        Vector3i pos = new Vector3i(xPos, y, zPos);

                        int yMax = mesh.HeightMap[Hasher.Hash(xPos, zPos)];

                        if (y > yMax) layer.AddBlock(Hasher.Hash(xPos, y, zPos), new Block(pos, BlockID.Air, true));
                        else layer.AddBlock(Hasher.Hash(xPos, y, zPos), new Block(pos, BlockID.Grass));
                    }
                _Layers[y] = layer;
            }
        }

        public static float[] BackFace(int x, int y, int z)
        {
            return new float[] {
                -BLOCK_SIZE + x, -BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                BLOCK_SIZE + x, -BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                BLOCK_SIZE + x, BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                -BLOCK_SIZE + x, BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            };
        }

        public static float[] RightFace(int x, int y, int z)
        {
            return new float[] {
                BLOCK_SIZE + x, -BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                BLOCK_SIZE + x, -BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                BLOCK_SIZE + x, BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                BLOCK_SIZE + x, BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        };
        }

        public static float[] FrontFace(int x, int y, int z)
        {
            return new float[] {
                BLOCK_SIZE + x, -BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                -BLOCK_SIZE + x, -BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                -BLOCK_SIZE + x, BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                BLOCK_SIZE + x, BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        };
        }

        public static float[] LeftFace(int x, int y, int z)
        {
            return new float[] {
                -BLOCK_SIZE + x, -BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                -BLOCK_SIZE + x, -BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                -BLOCK_SIZE + x, BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                -BLOCK_SIZE + x, BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        };
        }

        public static float[] TopFace(int x, int y, int z)
        {
            return new float[] {
                -BLOCK_SIZE + x, BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                BLOCK_SIZE + x, BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                BLOCK_SIZE + x, BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                -BLOCK_SIZE + x, BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        };
        }

        public static float[] BottomFace(int x, int y, int z)
        {
            return new float[] {
                -BLOCK_SIZE + x, -BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                BLOCK_SIZE + x, -BLOCK_SIZE + y, -BLOCK_SIZE + z, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                BLOCK_SIZE + x, -BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                -BLOCK_SIZE + x, -BLOCK_SIZE + y, BLOCK_SIZE + z, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        };
        }
    }
}
