using System;
using static Qube.Enviro.WorldConstants;

namespace Qube.Util.Noise
{
    public class Noise
    {
        private int _Width;
        private int _Height;
        private double[] _Values;
        private static Random _Random = new Random();

        public double this[int index]
        {
            get
            {
                return _Values[index];
            }
        }

        public Noise(int width, int heihgt)
        {
            _Width = width;
            _Height = heihgt;

            _Values = new double[_Width * _Height];

            int stepSize = _Width;
            double scale = 1.0 / _Width;
            do
            {
                int halfStep = stepSize / 2;
                for (int y = 0; y < _Width; y += stepSize)
                    for (int x = 0; x < _Width; x += stepSize)
                    {
                        double a = Sample(x, y);
                        double b = Sample(x + stepSize, y);
                        double c = Sample(x, y + stepSize);
                        double d = Sample(x + stepSize, y + stepSize);

                        double e = (a + b + c + d) / 4.0 + (_Random.NextDouble() * 2 - 1) * stepSize * scale;
                        SetSample(x + halfStep, y + halfStep, e);
                    }
                for (int y = 0; y < _Width; y += stepSize)
                    for (int x = 0; x < _Width; x += stepSize)
                    {
                        double a = Sample(x, y);
                        double b = Sample(x + stepSize, y);
                        double c = Sample(x, y + stepSize);
                        double d = Sample(x + stepSize, y + stepSize);
                        double e = Sample(x + halfStep, y - halfStep);
                        double f = Sample(x - halfStep, y + halfStep);

                        double h = (a + b + d + e) / 4.0 + (_Random.NextDouble() * 2 - 1) * stepSize * scale;
                        double g = (a + c + d + f) / 4.0 + (_Random.NextDouble() * 2 - 1) * stepSize * scale;
                        SetSample(x + halfStep, y, h);
                        SetSample(x, y + halfStep, g);
                    }
                stepSize /= 2;
            } while (stepSize > 1);
        }

        private double Sample(int x, int y)
        {
            return _Values[(x & (_Width - 1)) + (y & (_Height - 1)) * _Width];
        }

        private void SetSample(int x, int y, double value)
        {
            _Values[(x & (_Width - 1)) + (y & (_Height - 1)) * _Width] = value;

        }
    }
}
