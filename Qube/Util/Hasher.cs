using System;

namespace Qube.Util
{
    public class Hasher
    {
        public static int Hash(int x, int y, int z) => HashCode.Combine(x, y, z);
        public static int Hash(int x, int y) => HashCode.Combine(x, y);
    }
}
