#type vertex
#version 450 core

layout(location = 0) in vec4 a_Position;
layout(location = 1) in vec4 a_Color;

uniform mat4 u_ProjectionView;
uniform mat4 u_Transform;

out vec4 v_Color;

void main()
{
    v_Color = a_Color;
    gl_Position = u_ProjectionView * u_Transform * a_Position;
}

#type fragment
#version 450 core

layout(location = 0) out vec4 FragColor;

in vec4 v_Color;

void main()
{
    // FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    FragColor = v_Color;
}
