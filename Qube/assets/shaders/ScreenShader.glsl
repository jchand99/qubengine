#type vertex
#version 450 core

layout(location = 0) in vec2 a_Position;
layout(location = 1) in vec2 a_TexCoords;

out vec2 v_TexCoords;

void main()
{
    gl_Position = vec4(a_Position, 0.0, 1.0);
    v_TexCoords = a_TexCoords;
}

#type fragment
#version 450 core

out vec4 FragColor;

in vec2 v_TexCoords;

uniform sampler2D u_screenTexture;

void main()
{
    FragColor = texture(u_screenTexture, v_TexCoords);
}
